/*
 Navicat Premium Data Transfer

 Source Server         : laragon-mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : panti

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 14/09/2019 19:50:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for banks
-- ----------------------------
DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_holder` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of banks
-- ----------------------------
INSERT INTO `banks` VALUES (1, 'BCA', '7003220978', 'Panti Al-Kautsar', 100, '2019-08-29 21:00:12', '2019-09-14 12:10:55');
INSERT INTO `banks` VALUES (2, 'Mandiri', '13100099922233', 'Panti Al-Kautsar', 100, '2019-08-29 14:15:52', '2019-09-14 12:11:04');
INSERT INTO `banks` VALUES (3, 'BRI', '145145145899', 'Panti Al-Kautsar', 100, '2019-09-14 12:11:22', '2019-09-14 12:11:22');
INSERT INTO `banks` VALUES (4, 'BNI', '989556333356', 'Panti Al-Kautsar', 0, '2019-09-14 12:11:43', '2019-09-14 12:11:50');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Kegiatan', 'kegiatan', 'Ini kategori untuk artikel kegiatan', 100, '2019-09-07 20:37:21', NULL);
INSERT INTO `categories` VALUES (2, 'Berita', 'berita', 'Kategori untuk artikel tentang berita', 100, '2019-09-07 13:43:27', '2019-09-07 13:45:31');

-- ----------------------------
-- Table structure for confirmations
-- ----------------------------
DROP TABLE IF EXISTS `confirmations`;
CREATE TABLE `confirmations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `donation_id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `account_holder` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal_transfer` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `admin_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of confirmations
-- ----------------------------
INSERT INTO `confirmations` VALUES (1, 1, 1, 'Muhamad Iqbal', 8000000, '2019-08-30', '-', 1, 'sample.jpg', 200, '2019-08-30 17:02:52', '2019-08-30 14:45:47');
INSERT INTO `confirmations` VALUES (2, 2, 1, 'Nabilah Ayu', 5000000, '2019-08-31', '-', 1, 'sample.jpg', 100, '2019-08-30 17:02:52', '2019-08-30 14:51:19');
INSERT INTO `confirmations` VALUES (3, 1, 1, 'Muhamad Iqbal', 8000000, '2019-08-31', '-', 1, 'sample.jpg', 10, '2019-08-30 17:02:52', '2019-08-30 14:45:19');
INSERT INTO `confirmations` VALUES (4, 4, 1, 'Nabilah Ayu', 1000000, '2019-09-14', NULL, NULL, 'c9a3a110dd523b098dcd0b5c6549a6d3.jpg', 100, '2019-09-14 11:56:18', '2019-09-14 11:56:18');
INSERT INTO `confirmations` VALUES (6, 5, 3, 'Dadang Dimas', 500000, '2019-09-14', 'Semoga bermanfaat', NULL, 'eec83e026ab45d265ae4301eb62cd574.jpg', 100, '2019-09-14 12:38:32', '2019-09-14 12:38:32');

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'Muhamad Iqbal', 'muhamad.iqbal@gmail.com', '089692825421', 'Test Subject', 'Test pesan', '2019-09-13 13:36:14', '2019-09-13 13:36:14');
INSERT INTO `contacts` VALUES (2, 'Nabilah Ayu', 'nabilah@gmail.com', '089692825765', 'Test Subject 2', 'Test Isi pesan', '2019-09-13 13:40:33', '2019-09-13 13:40:33');

-- ----------------------------
-- Table structure for donations
-- ----------------------------
DROP TABLE IF EXISTS `donations`;
CREATE TABLE `donations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nominal` int(10) UNSIGNED NOT NULL,
  `category` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `status` int(50) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `donations_code_unique`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of donations
-- ----------------------------
INSERT INTO `donations` VALUES (1, 'D75EB700', NULL, 'Bapak', 'Iqbal Ramadhan', 'iqbal@gmail.com', '087221765233', 'Bandung', 8000000, 'Zakat', 1, 200, '2019-08-29 21:56:39', '2019-08-30 14:16:29');
INSERT INTO `donations` VALUES (2, '02D09017', NULL, 'Saudari', 'Nabilah Ayu', 'nabilah@gmail.com', '089692825765', 'Jakarta Selatan', 5000000, 'Infaq', 1, 100, '2019-08-30 07:06:20', '2019-08-30 14:51:19');
INSERT INTO `donations` VALUES (3, '06494A9B', NULL, 'Bapak', 'Satria Yoga', 'satria@gmail.com', '0896926565611', 'Cibiru - Bandung', 1000000, 'Shodaqoh', 1, 100, '2019-09-11 12:56:48', '2019-09-11 12:56:48');
INSERT INTO `donations` VALUES (4, '2656D995', 4, 'Saudari', 'Nabilah Ayu', 'nabilah@gmail.com', '089692825765', 'Jakarta', 1000000, 'Infaq', 1, 100, '2019-09-14 07:27:43', '2019-09-14 07:27:43');
INSERT INTO `donations` VALUES (5, '454BD254', NULL, 'Bapak', 'Dadang', 'dadang@gmail.com', '89692456780', 'Cibiru - Bandung', 500000, 'Shodaqoh', 3, 100, '2019-09-14 12:16:08', '2019-09-14 12:16:08');
INSERT INTO `donations` VALUES (6, 'BEC9C5C5', NULL, 'Bapak', 'Dadang', 'dadang@gmail.com', '896928267677', 'Perum Griya Mitra Blok F2 no.34', 1000000, 'Infaq', 2, 100, '2019-09-14 12:39:04', '2019-09-14 12:39:04');

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `case_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stay_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES (1, 'Michael Valentino', 'Laki-laki', '14 Tahun', 'Anak Terlantar', 'Masih ada', 'Panti', NULL, 100, '2019-08-29 19:15:00', NULL);
INSERT INTO `members` VALUES (2, 'Lulu Tsania', 'Perempuan', '12 Tahun', 'Anak Terlantar', 'Masih ada', 'Panti', NULL, 100, '2019-08-29 13:28:53', '2019-08-29 13:35:39');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_27_043642_laratrust_setup_tables', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_27_045611_create_banks_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_08_27_045631_create_donations_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_08_27_050218_create_confirmations_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_08_27_054342_create_categories_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_08_27_054358_create_posts_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_08_27_054422_create_pages_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_08_27_140106_create_members_table', 2);
INSERT INTO `migrations` VALUES (11, '2019_09_13_132136_create_contacts_table', 3);

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, 1, 'Kata Pengantar', 'kata-pengantar', '<p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">Puji syukur kami panjatkan kehadirat Allah SWT yang telah melimpahkan rahmat, taufik dan hidayah-Nya sehingga kami dapat menyusun dan menyelesaikan pembuatan buku &nbsp;tentang Profil LKSA/PSAA e-Panti.</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">Profil ini memuat tentang gambaran umum LKSA/PSAA e-Panti termasuk program perlindungan dan pelayanan kesejahteraan sosial anak baik yang berada dalam lembaga maupun yang berada dalam asuhan keluarga</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">Pada kesempatan ini, kami menyampaikan terima kasih yang sebesar-besarnya kepada Pimpinan Yayasan Esa e-Panti Bhakti serta seluruh Pengurus dan staf LKSA/PSAA e-Panti yang telah membantu dan berpartisipasi dalam penyelesaian penyusunan profil ini.</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">Kami menyadari bahwa profil ini masih banyak yang perlu disempurnakan sehingga diharapkan masukan dan saran yang membangun dan konstruktif dalam menyempurnakan profil ini di masa yang akan datang.</span></p>&nbsp;&nbsp;<p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: right;\"><span style=\"font-weight: 700;\">Bandung, 01 Juli 2019</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: right;\"><span style=\"font-weight: 700;\">Kepala LKSA/PSAA &nbsp;e-Panti</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: right;\"><span style=\"font-weight: 700;\"><u style=\"font-size: 1rem;\">Test Testing, S.Pd. MPd</u></span></p>', NULL, 100, '2019-09-13 13:04:19', '2019-09-13 13:05:13');
INSERT INTO `pages` VALUES (2, 1, 'Visi & Misi', 'visi-misi', '<p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: center;\"><span style=\"font-weight: 700;\">VISI</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: center;\"><span style=\"font-weight: 700;\">Membina dan Membangun Kepribadian Anak Asuh Yang Berakhlakul Karimah dan Mandiri.</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: center;\"><br><span style=\"font-weight: 700;\">MISI</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); text-align: center;\"><span style=\"font-weight: 700;\">Meningkatkan Kesejahteraan dan Kemandirian Anak asuh Dalam Menyongsong Kehidupan Bermasyarakat Dimasa Depan.</span></p>', NULL, 100, '2019-09-13 13:06:03', '2019-09-13 13:06:03');
INSERT INTO `pages` VALUES (3, 1, 'Sarana dan Prasarana', 'sarana-dan-prasarana', '<p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">1. Tanah :&nbsp; 2.492&nbsp;&nbsp;&nbsp;&nbsp; m2</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">2. Gedung Panti :&nbsp;&nbsp;&nbsp; &nbsp;600&nbsp;&nbsp;&nbsp;&nbsp; m2</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">3. Sekolah :&nbsp;&nbsp; 1800 &nbsp;&nbsp;&nbsp;&nbsp; m2</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">4. Gedung</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">a. Gedung Kantor :&nbsp;&nbsp;2 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">b. Ruang Pekerja Sosial :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">c. Ruang Serba Guna / Aula :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">d. Gudang :&nbsp; 2 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">e. Kamar Mandi : 10 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">5. Kamar</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">a. Kamar Pengasuh :&nbsp; 3 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">b. Kamar Tamu :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">c. Kamar Anak Perempuan :&nbsp; 6 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">d. Kamar Anak Laki-laki :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">6. Ruang Pelayanan</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">a. Ruang Konseling :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">b. Ruang Belajar : 16 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">c. Ruang Pelatihan Keterampilan :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">d. Ruang Perpustakaan :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">e. Ruang Ibadah :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">f. Tempat / Ruang Bermain dan Olahraga :&nbsp; 1 unit</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89); padding-left: 30px;\"><span style=\"font-weight: 700;\">g. Ruang Makan / Dapur :&nbsp; 1 unit</span></p>', NULL, 100, '2019-09-13 13:06:31', '2019-09-13 13:06:31');
INSERT INTO `pages` VALUES (4, 1, 'Struktur Organisasi', 'struktur-organisasi', '<p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Ketua Yayasan&nbsp; :<br>H. Abdul Muas, BSc</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Kepala LKSA/PSAA :<br>H. Agus Sutardi, S.Pd. MPd</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Sekretaris :<br>Eko Firmansyah, ST. &nbsp;M.Pd</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Wakil Sekretaris&nbsp;</span><span style=\"font-weight: 700;\">:<br>Mulyana, S.Sos</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Bendahara :<br>Hj. Ukasih Yuna</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Wakil Bendahara :<br>Poppi Sri Lestari, M.Si</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Seksi Pendidikan :<br>Atep Rohmana, S.Pd.I,<br>N. Khomisah, S.Ag,<br></span><span style=\"font-weight: 700;\">Dede Rusmana, S.Pd.I</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Seksi Rumah Tangga :<br>Hj. Suiswaty Muas,<br>Hj. Ecin Kuraesin,<br>Imas K,<br></span><span style=\"font-weight: 700;\">Bardi Daryaman</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Seksi Peksos/Humas :<br>Yayat Ruhiyat, S.Pd.I,<br>Hj. Ai Herawati</span></p><br style=\"color: rgb(89, 89, 89); font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;\"=\"\"><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: \" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" padding-bottom:=\"\" 0px;=\"\" color:=\"\" rgb(89,=\"\" 89,=\"\" 89);\"=\"\"><span style=\"font-weight: 700;\">Pekerja Sosial :<br>Gilang Susalit, S.ST,</span></p>', NULL, 100, '2019-09-13 13:06:55', '2019-09-13 13:07:49');
INSERT INTO `pages` VALUES (5, 1, 'Kerja Sama', 'kerja-sama', '<p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">1. Lembaga Perekonomian : BCA, BRI, BJB, Bank Mandiri</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">2. Lembaga Pendidikan :</span></p><ol style=\"margin-bottom: 10px; color: rgb(89, 89, 89); font-family: &quot;Source Sans Pro&quot;, sans-serif;\"><li style=\"list-style-type: none;\"><ol><li style=\"list-style-type: none;\"><ul><li><span style=\"font-weight: 700;\">Bina Sarana Informatika (BSI)</span></li><li><span style=\"font-weight: 700;\">Universitas Sangga Buana (USB)</span></li><li><span style=\"font-weight: 700;\">Akademi Akuntansi Bandung</span></li><li><span style=\"font-weight: 700;\">Universitas Islam Negeri (UIN) Sunan Gunung Djati Bandung</span></li><li><span style=\"font-weight: 700;\">Sekolah Tinggi kesejahteraan Sosial (STKS) Bandung</span></li><li><span style=\"font-weight: 700;\">STIKES Bhakti Husada</span></li></ul></li></ol></li></ol><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">3. Lembaga Kesehatan :</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">Puskesmas Mohammad Ramdhan, Puskesmas Pasundan, Rumah Sakit Hasan Sadikin (RSHS)</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">4. Instansi :</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">Kantor Kelurahan, Kantor Kecamatan, Polsek, &nbsp;Koramil, Dinas Sosial Kota Bandung, Dinas Sosial Propinsi Jawa Barat, PLN Cabang Bandung, Save The Chidren, International Organisasi For Migration (IOM)</span></p><p style=\"margin-right: 0px; margin-bottom: 1px; margin-left: 0px; line-height: 26px; opacity: 0.95; font-family: &quot;Source Sans Pro&quot;, sans-serif; padding-bottom: 0px; color: rgb(89, 89, 89);\"><span style=\"font-weight: 700;\">5. Dunia Usaha (Perusahaan-perusahaan) yang mau menampung lulusan dari LKSA/PSAA yang sudah menamatkan jenjang pendidikan SMA).</span></p>', NULL, 100, '2019-09-13 13:07:34', '2019-09-13 13:07:34');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `permission_role_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permission_user
-- ----------------------------
DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE `permission_user`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`, `permission_id`, `user_type`) USING BTREE,
  INDEX `permission_user_permission_id_foreign`(`permission_id`) USING BTREE,
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `permissions_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `view_count` int(10) NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 1, 1, 'Test Post', 'test-post-1', '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\"><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Sed id est in libero sodales fringilla in fringilla ex. Integer malesuada lectus eget nunc dignissim placerat. Mauris nisi urna, scelerisque vel dignissim ac, aliquet id nulla. Morbi id mauris ac neque molestie varius ac vitae sem. Proin ligula risus, porttitor congue euismod venenatis, ullamcorper vel augue. Nam pharetra elit eu sem venenatis, et venenatis metus venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum feugiat et mi vel auctor. Quisque sit amet porttitor dui, auctor commodo quam. Vivamus porta urna et sem posuere mattis.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Quisque ultrices efficitur nibh sit amet faucibus. Morbi at facilisis est, et tristique lacus. Suspendisse et luctus tortor. Nulla nec semper augue, in accumsan neque. In eu massa sed metus condimentum pulvinar. Aliquam erat volutpat. Suspendisse condimentum orci nec condimentum feugiat. Morbi consequat tempor nisl auctor aliquet. Aliquam semper pharetra congue. Etiam gravida enim in porttitor aliquet. Donec gravida congue est. Phasellus lacinia urna et magna sollicitudin, non bibendum risus scelerisque. Aliquam nec lectus nisl. Ut aliquam purus eu risus volutpat consectetur.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Aenean laoreet semper diam, vel rutrum eros pulvinar et. Cras volutpat, felis sed maximus interdum, tortor enim convallis augue, sollicitudin euismod risus odio sed ex. Nam eget ornare nisi, efficitur aliquam ante. Vivamus euismod augue ac urna dictum, eu rutrum purus rutrum. Vestibulum quis lorem felis. Duis sagittis est mi, eget mattis turpis gravida sollicitudin. Proin semper ligula at diam egestas aliquet. Fusce sed egestas lacus. Cras vestibulum dignissim diam sed pharetra. Curabitur vitae aliquam magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultrices condimentum turpis. Vivamus nec justo ornare metus scelerisque lobortis. Phasellus pulvinar libero nulla, at lobortis lacus ultrices euismod.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer condimentum vestibulum dictum. Fusce sit amet magna vel lacus dignissim vulputate ac ut ipsum. Phasellus molestie lacinia odio a ornare. Suspendisse congue massa dui, in tincidunt augue consequat in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus eget pellentesque erat, vel ornare orci.</p>', '1e95098021eff814b3a8dc3e95ae8bad.jpg', 100, 0, '2019-09-07 21:01:00', '2019-09-14 12:08:38');
INSERT INTO `posts` VALUES (2, 1, 2, 'Jeruk Adalah Buah', 'jeruk-adalah-buah-1', '<p class=\"note\" style=\"box-sizing: inherit; margin: 1em 30px 1em 0px; color: rgb(136, 136, 136); padding: 3px 0px 3px 15px; border-left: 3px solid rgb(255, 184, 100); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 18px;=\"\" background-color:=\"\" rgb(244,=\"\" 247,=\"\" 250);\"=\"\">Jeruk Adalah Buah</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id est in libero sodales fringilla in fringilla ex. Integer malesuada lectus eget nunc dignissim placerat. Mauris nisi urna, scelerisque vel dignissim ac, aliquet id nulla. Morbi id mauris ac neque molestie varius ac vitae sem. Proin ligula risus, porttitor congue euismod venenatis, ullamcorper vel augue. Nam pharetra elit eu sem venenatis, et venenatis metus venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum feugiat et mi vel auctor. Quisque sit amet porttitor dui, auctor commodo quam. Vivamus porta urna et sem posuere mattis.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Quisque ultrices efficitur nibh sit amet faucibus. Morbi at facilisis est, et tristique lacus. Suspendisse et luctus tortor. Nulla nec semper augue, in accumsan neque. In eu massa sed metus condimentum pulvinar. Aliquam erat volutpat. Suspendisse condimentum orci nec condimentum feugiat. Morbi consequat tempor nisl auctor aliquet. Aliquam semper pharetra congue. Etiam gravida enim in porttitor aliquet. Donec gravida congue est. Phasellus lacinia urna et magna sollicitudin, non bibendum risus scelerisque. Aliquam nec lectus nisl. Ut aliquam purus eu risus volutpat consectetur.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer condimentum vestibulum dictum. Fusce sit amet magna vel lacus dignissim vulputate ac ut ipsum. Phasellus molestie lacinia odio a ornare. Suspendisse congue massa dui, in tincidunt augue consequat in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus eget pellentesque erat, vel ornare orci.</p>', '018916c6ed5484349e44e872ff841304.jpg', 100, 0, '2019-09-07 15:04:19', '2019-09-14 12:09:51');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user`  (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`, `user_type`) USING BTREE,
  INDEX `role_user_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES (1, 1, 'App\\User');
INSERT INTO `role_user` VALUES (1, 3, 'App\\User');
INSERT INTO `role_user` VALUES (2, 2, 'App\\User');
INSERT INTO `role_user` VALUES (2, 4, 'App\\User');
INSERT INTO `role_user` VALUES (2, 5, 'App\\User');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'Administrator', NULL, '2019-08-27 06:41:01', '2019-08-27 06:41:01');
INSERT INTO `roles` VALUES (2, 'donatur', 'Donatur', NULL, '2019-08-27 06:41:01', '2019-08-27 06:41:01');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Admin Panti', 'Bapak', 'Bandung', '082111444777', 'admin@gmail.com', NULL, 100, NULL, '$2y$10$.jtcmsrVPqk/4Er8pDK5i.7LSpALkUemRlfYGuHD3Ewwiczutbsqa', '9SgyK8EIfTKYLdwhdCbcj1GCqlQFy7lET3kqzqRyOcp8xeGURF9niFg2VbHk', '2019-08-27 06:41:01', '2019-09-01 12:58:20');
INSERT INTO `users` VALUES (2, 'Sample User', 'Bapak', 'Jakarta Pusat', '089666444222', 'user@gmail.com', NULL, 100, NULL, '$2y$10$Ey0b2Om6wtS2cP4xb7RJ6.MNT00H6NziHNHvet//bPKfFkDu229PC', 'u4ajx3hS0HZZkwt7Pv3HM6gpF4miTKrxpshMkW7WTdbax9jkURcr7YI8Le4p', '2019-08-27 06:41:01', '2019-08-27 12:48:23');
INSERT INTO `users` VALUES (3, 'Muhamad Iqbal', 'Saudara', 'Cibiru - Bandung', '089692825280', 'iqbal@gmail.com', 'dc4eae1ac4f38395bd961c328f68e9be.png', 100, NULL, '$2y$10$hjq3auep6AiSWmuNblD9puFwUX67yrGJRi7lAcgPbJD9PXO15yKqS', 'yD5T38PAmoTGe005EB0RCOPkUoocjBAWmotNF4pRgF0tdRwN3pmKGKBKajR0', '2019-08-27 12:37:53', '2019-09-01 13:24:46');
INSERT INTO `users` VALUES (4, 'Nabilah Ayu', 'Saudari', 'Jakarta', '089692825765', 'nabilah@gmail.com', NULL, 100, NULL, '$2y$10$zla0fokswWU.HGTn2y4VSeJzKCy3PMt/9DCeV3lzR6/yjz6wlSJTW', 'BTt5K08HekxhdCZbs8oeZ25aRgUqyfE49lGlICXrSiTwWeMJZBEu0Lz4eReQ', '2019-09-11 07:48:16', '2019-09-11 07:49:06');
INSERT INTO `users` VALUES (5, 'Melody', 'Saudari', NULL, NULL, 'melody@gmail.com', NULL, 100, NULL, '$2y$10$SQ0jxCoND.nnp79d50QrruhG9PJ7rk1zjbhe7/dKHyrFOxSTF9ZY.', '9z8T5GJt3uUZfi74EI1fxs4G0NoyEvUjLe8IASuyDfBPT1CmuOc2UfRDlzTH', '2019-09-11 07:50:04', '2019-09-11 07:50:04');

SET FOREIGN_KEY_CHECKS = 1;
