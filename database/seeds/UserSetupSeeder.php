<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\User;

class UserSetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		// Membuat role admin
		$adminRole = new Role();
		$adminRole->name = "admin";
		$adminRole->display_name = "Administrator";
		$adminRole->save();

		// Membuat role member
		$memberRole = new Role();
		$memberRole->name = "user";
		$memberRole->display_name = "Normal User";
		$memberRole->save();

		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin Panti';
		$admin->email = 'admin@gmail.com';
		$admin->password = Hash::make('123qwerty');
		$admin->save();
		$admin->attachRole($adminRole);

		// Membuat sample member
		$member = new User();
		$member->name = "Sample User";
		$member->email = 'user@gmail.com';
		$member->password = Hash::make('123qwerty');
		$member->save();
		$member->attachRole($memberRole);
	}
}
