<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
	protected $appends = ['display_status'];

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Aktif";
        } else if ($value == 0) {
            $status = "Non Aktif";
        } else {
            $status = "Tidak Ada";
        }
        
        return $status;
    }
}
