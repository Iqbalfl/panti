<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use App\User;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $users = User::with('roles');

            if ($request->get('role') != null) {
                if ($request->role != 'all') {
                    $users->whereRoleIs($request->role);
                }
            }

            $users = $users->select('users.*');
            
            return Datatables::of($users)
                ->addIndexColumn()
                ->addColumn('action', function($user){
                    return view('partials._action', [
                        'model'           => $user,
                        'form_url'        => route('user.destroy', $user->id),
                        'edit_url'        => route('user.edit', $user->id)
                    ]);
                })
                ->addColumn('role_name', function ($user) {
                    return ($user->roles[0]->display_name);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('users.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|string',
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users',
          'phone' => 'required|numeric',
          'address' => 'required|string',
          'password' => 'required|string|min:6|confirmed',
          'role' => 'required|string',
        ]);
        
        $user = new User;
        $user->title = $request->title;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->password =  Hash::make($request->password);
        $user->save();

        $user->attachRole($request->role);

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Akun dengan nama $user->name berhasil dibuat"
        ]);
        
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('users.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'title' => 'required|string',
          'name' => 'required|string|max:255',
          'email' => 'required|unique:users,email,' . $id,
          'phone' => 'required|numeric',
          'address' => 'required|string',
          'role' => 'required|string',
        ]);
        
        $user = User::find($id);
        $user->title = $request->title;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->save();

        if ($user->roles[0]->name != $request->role) {
            $user->detachRole($user);
            $user->attachRole($request->role);
        }


        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Akun dengan nama $user->name berhasil diubah"
        ]);
        
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user->roles) {
            $user->detachRole($user);
        }

        $user->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('user.index');
    }

    public function showProfile()
    {
        $user = \Auth::user();

        return view('users.profile', ['user' => $user]);
    }

    public function updateProfile(Request $request)
    {
        $auth = \Auth::user()->id;
        $user = User::find($auth);
        
        $this->validate($request, [
            'title' => 'required|string',
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users,email,' . $auth,
            'phone' => 'required|numeric',
            'address' => 'required|string',
            'avatar' => 'image|max:1024',
        ]);

        if ($request->hasFile('avatar')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('avatar');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($user->avatar) {
                $old_image = $user->avatar;
                if ($user->avatar != 'user-default.jpg') {
                    # delete if not default
                    $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
                     . DIRECTORY_SEPARATOR . $user->avatar;
                    try {
                        File::delete($filepath);
                    } catch (FileNotFoundException $e) {
                        // File sudah dihapus/tidak ada
                    }
                }
            }
            // ganti field image dengan image yang baru
            $user->avatar = $filename;
        }

        $user->name = $request->name;
        $user->title = $request->title;
        $user->address = $request->address;        
        $user->phone = $request->phone;        
        $user->email = $request->email;        
        $user->save();
    
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Profil berhasil diubah"
        ]);

        return redirect()->route('profile.show');
    }
}
