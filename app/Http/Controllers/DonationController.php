<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Donation;
use Session;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $donations = Donation::with('bank');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $donations->where('status', $request->status);
                }
            }

            $donations = $donations->select('donations.*');
            
            return Datatables::of($donations)
                ->addIndexColumn()
                ->addColumn('action', function($donation){
                    return view('partials._action', [
                        'model'           => $donation,
                        'form_url'        => route('donation.destroy', $donation->id),
                        'edit_url'        => route('donation.edit', $donation->id),
                        'show_url'        => route('donation.show', $donation->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('donations.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('donations.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|string',
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255',
          'phone' => 'required|numeric',
          'address' => 'required|string',
          'nominal' => 'required|numeric',
          'category' => 'required|string',
          'bank_id' => 'required|numeric',
        ]);
        
        $donation = new Donation;
        $donation->code = Donation::generateCode();
        $donation->title = $request->title;
        $donation->name = $request->name;
        $donation->email = $request->email;
        $donation->phone = $request->phone;
        $donation->address = $request->address;
        $donation->nominal = $request->nominal;
        $donation->category = $request->category;
        $donation->bank_id = $request->bank_id;
        $donation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Donasi berhasil dibuat"
        ]);
        
        return redirect()->route('donation.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $donation = Donation::find($id);

        return view('donations.show')->with(compact('donation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $donation = Donation::find($id);

        return view('donations.edit')->with(compact('donation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'title' => 'required|string',
          'name' => 'required|string|max:255',
          'email' => 'required|string|email',
          'phone' => 'required|numeric',
          'address' => 'required|string',
          'nominal' => 'required|numeric',
          'category' => 'required|string',
          'bank_id' => 'required|numeric',
        ]);
        
        $donation = Donation::find($id);
        $donation->title = $request->title;
        $donation->name = $request->name;
        $donation->email = $request->email;
        $donation->phone = $request->phone;
        $donation->address = $request->address;
        $donation->nominal = $request->nominal;
        $donation->category = $request->category;
        $donation->bank_id = $request->bank_id;
        $donation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Donasi berhasil diedit"
        ]);
        
        return redirect()->route('donation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donation = Donation::find($id);

        if ($donation->status == 200) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat menghapus donasi yang status nya sudah diterima!"
            ]);

            return redirect()->back();
        }

        $donation->confirmations()->delete();

        $donation->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('donation.index');
    }
}
