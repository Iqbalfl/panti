<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Media;

class GalleryController extends Controller
{
	public function index(Request $request)
	{
		$galleries = Media::get();

		return view('galleries.index')->with(compact('galleries'));
	}

	public function destroy(Request $request)
    {
        $id = $request->media_id;

        $gallery = Media::find($id);

        if ($gallery){
            $path = $gallery->path.'/'.$gallery->name;
            
            if (file_exists($path)) {
                File::delete($path);
            }

            $gallery->delete();

            $result = ['message' => 'success', 'media_id' => $id];
        } else {
            $result = ['message' => 'failed', 'media_id' => 'not found'];
        }

        return response()->json($result);
    }


 	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dropZoneStore(Request $request)
    {
        $path = 'uploads/images/media';
        $image_path = public_path($path);

        if(!File::isDirectory($image_path)){
            File::makeDirectory($image_path, 0777, true, true);
        }

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        //$resizeName = '720_'.$imageName;

        // resize photo and save
        Image::make($image)
            ->resize(1080, 1080, function ($constraints) {
                $constraints->aspectRatio();
            })
            ->save($image_path . DIRECTORY_SEPARATOR . $imageName);
        
        // save original size
        // $image->move($image_path, $imageName);

        // store to db
        $imageUpload = new Media();
        $imageUpload->name = $imageName;
        $imageUpload->type = 100;
        $imageUpload->path = $path;
        $imageUpload->save();

        return response()->json(['message' => 'Save Success','media_name' => $imageName, 'media_id' => $imageUpload->id]);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dropZoneDestroy(Request $request)
    {
        $filename = $request->get('filename');
        
        $media = Media::where('name', $filename)->first();

        $media_id = $media->id;

        $path = $media->path . DIRECTORY_SEPARATOR . $filename;
        
        if (file_exists($path)) {
            unlink($path);
        }

        $media->delete();

        return response()->json(['message' => 'Delete Success','media_name' => $filename, 'media_id' => $media_id]);
    }
}
