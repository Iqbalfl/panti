<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Confirmation;
use App\Donation;
use Session;

class ConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $confirmations = Confirmation::query();

        $date = null;
        if ($request->has('date')) {
            $date = $request->date;
            if ($date != null && $date != '') {
                $raw_date = explode('-',$date);        
                $start_date = \Carbon\Carbon::parse($raw_date[0])->format('Y-m-d');
                $end_date = \Carbon\Carbon::parse($raw_date[1])->format('Y-m-d');

                $confirmations->whereBetween('date', [$start_date, $end_date]);
            }
        }

        $status = null;
        if ($request->has('status')) {
            $status = $request->status;
            if ($status != 'all') {
               $confirmations->where('status', $status);
            }
        }

        $confirmations = $confirmations->paginate(10);

        return view('confirmations.index')->with(compact('confirmations', 'date', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $confirmation = Confirmation::find($id);
        $donation = Donation::find($confirmation->donation_id);

        if ($donation->status == 200 || $confirmation->status == 200) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Donasi dengan kode $donation->code sudah di approve sebelumnya!"
            ]);

            return redirect()->back();
        }

        $donation->status = 200;
        $donation->save();

        $confirmation->status = 200;
        $confirmation->admin_id = \Auth::user()->id;
        $confirmation->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil melakukan konfirmasi"
        ]);

        return redirect()->route('confirmation.index');
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, $id)
    {
        $confirmation = Confirmation::find($id);
        $donation = Donation::find($confirmation->donation_id);

        if ($confirmation->status == 200) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat melakukan pembatalan konfirmasi yang sudah diterima!"
            ]);

            return redirect()->back();
        }

        if ($confirmation->status == 10) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Konfirmasi sudah di cancel sebelumnya!"
            ]);

            return redirect()->back();
        }

        $is_confirmed = false;

        foreach ($donation->confirmations as $item) {
            if ($item->status == 200) {
                $is_confirmed = true;
            }
        }

        if ($is_confirmed == false) {
            $donation->status = 10;
            $donation->save();
        }

        $confirmation->status = 10;
        $confirmation->admin_id = \Auth::user()->id;
        $confirmation->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil melakukan pembatalan"
        ]);

        return redirect()->route('confirmation.index');
    }

}
