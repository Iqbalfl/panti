<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;

use App\Donation;
use App\Post;
use App\Member;
use App\Contact;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Laratrust::hasRole('admin')) return $this->adminDashboard();
        if (Laratrust::hasRole('donatur')) return $this->donaturDashboard();

        return abort(403);
    }

    protected function adminDashboard()
    {

        $donation = number_format(Donation::where('status', 200)->sum('nominal'),0,',','.');
        $member = Member::count();
        $post = Post::active()->count();
        $contact = Contact::count();

        $data = [
            'donation' => $donation,
            'member' => $member,
            'post' => $post,
            'contact' => $contact
        ];

        return view('dashboards.admin')->with(compact('data'));
    }

    protected function donaturDashboard()
    {
        $user_id = \Auth::user()->id;

        $success = number_format(Donation::user($user_id)->where('status', 200)->sum('nominal'),0,',','.');
        $pending = number_format(Donation::user($user_id)->where('status', 100)->sum('nominal'),0,',','.');

        $data = [
            'success' => $success,
            'pending' => $pending
        ];

        return view('dashboards.donatur')->with(compact('data'));
    }
}
