<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Member;
use Session;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $members = Member::query();
            $members = $members->select('*');
            
            return Datatables::of($members)
                ->addIndexColumn()
                ->addColumn('action', function($member){
                    return view('partials._action', [
                        'model'           => $member,
                        'form_url'        => route('member.destroy', $member->id),
                        'edit_url'        => route('member.edit', $member->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('members.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'gender' => 'required|string',
          'age' => 'required|string',
          'case_type' => 'required|string',
          'parent_status' => 'required|string',
          'stay_at' => 'required|string',
        ]);
        
        $member = new Member;
        $member->name = $request->name;
        $member->gender = $request->gender;
        $member->age = $request->age;
        $member->case_type = $request->case_type;
        $member->parent_status = $request->parent_status;
        $member->stay_at = $request->stay_at;
        $member->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Anak dengan nama $member->name berhasil dibuat"
        ]);
        
        return redirect()->route('member.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);

        return view('members.edit')->with(compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'gender' => 'required|string',
          'age' => 'required|string',
          'case_type' => 'required|string',
          'parent_status' => 'required|string',
          'stay_at' => 'required|string',
        ]);
        
        $member = Member::find($id);
        $member->name = $request->name;
        $member->gender = $request->gender;
        $member->age = $request->age;
        $member->case_type = $request->case_type;
        $member->parent_status = $request->parent_status;
        $member->stay_at = $request->stay_at;
        $member->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Anak dengan nama $member->name berhasil diedit"
        ]);
        
        return redirect()->route('member.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('member.index');
    }
}
