<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\Page;
use Session;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $pages = Page::with('user');

            $pages = $pages->select('*');
            
            return Datatables::of($pages)
                ->addIndexColumn()
                ->addColumn('action', function($page){
                    return view('partials._action', [
                        'model'           => $page,
                        'form_url'        => route('page.destroy', $page->id),
                        'edit_url'        => route('page.edit', $page->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('pages.index');    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|string|max:255',
          'content' => 'required',
          'image' => 'image|max:2048'
        ]);
        
        $page = new Page;
        $page->user_id = \Auth::user()->id;
        $page->title = $request->title;
        $page->slug = Page::generateSlug($request->title);
        $page->content = $request->content;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'pages';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // save nama field image
            $page->image = $filename;
        }

        $page->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Halaman dengan judul $page->title berhasil dibuat"
        ]);
        
        return redirect()->route('page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);

        return view('pages.edit')->with(compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'title' => 'required|string|max:255',
          'content' => 'required',
          'image' => 'image|max:2048'
        ]);
        
        $page = Page::find($id);
        $page->user_id = \Auth::user()->id;
        $page->title = $request->title;
        $page->content = $request->content;

        if ($page->title != $request->title) {
            $page->slug = Page::generateSlug($request->title);
        }

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'pages';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($page->image) {
                $old_image = $page->image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'pages'
                 . DIRECTORY_SEPARATOR . $page->image;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $page->image = $filename;
        }

        $page->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Halaman dengan judul $page->title berhasil diedit"
        ]);
        
        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);

        if ($page->image) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'pages'
             . DIRECTORY_SEPARATOR . $page->image;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        $page->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('page.index');
    }
}
