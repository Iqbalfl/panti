<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Donation;
use App\Confirmation;
use Session;

class DonaturController extends Controller
{
    public function indexDonation(Request $request)
    {
        if ($request->ajax()) {

            $donations = Donation::with('bank')->user(\Auth::user()->id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $donations->where('status', $request->status);
                }
            }

            $donations = $donations->select('donations.*');
            
            return Datatables::of($donations)
                ->addIndexColumn()
                ->addColumn('action', function($donation){
                    return view('partials._action', [
                        'model'           => $donation,
                        'show_url'        => route('donation.show.d', $donation->id),
                        'action_url'        => route('confirmation.create.d', $donation->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('donations.index-donatur'); 
    }

    public function showDonation($id)
    {
        $donation = Donation::find($id);

        return view('donations.show')->with(compact('donation'));
    }

    public function createDonation()
    {
    	$user = \Auth::user();
        return view('donations.create-donatur')->with(compact('user')); 
    }

    public function storeDonation(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|string',
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255',
          'phone' => 'required|numeric',
          'address' => 'required|string',
          'nominal' => 'required|numeric',
          'category' => 'required|string',
          'bank_id' => 'required|numeric',
        ]);
        
        $donation = new Donation;
        $donation->code = Donation::generateCode();
        $donation->user_id = \Auth::user()->id;
        $donation->title = $request->title;
        $donation->name = $request->name;
        $donation->email = $request->email;
        $donation->phone = $request->phone;
        $donation->address = $request->address;
        $donation->nominal = $request->nominal;
        $donation->category = $request->category;
        $donation->bank_id = $request->bank_id;
        $donation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Berhasil melakukan donasi, silahkan segera melakukan konfirmasi :)"
        ]);
        
        return redirect()->route('donation.index.d');
    }

    public function createConfirmation($donation_id)
    {
        $donation = Donation::find($donation_id);
        return view('confirmations.create')->with(compact('donation')); 
    }

    public function storeConfirmation(Request $request)
    {
        $this->validate($request, [
          'donation_id' => 'required|numeric',
          'bank_id' => 'required|numeric',
          'account_holder' => 'required|string',
          'date' => 'required|date',
          'nominal_transfer' => 'required|numeric',
          'image' => 'image|max:1024',
        ]);
        
        $confirmation = new Confirmation;;
        $confirmation->donation_id = $request->donation_id;
        $confirmation->bank_id = $request->bank_id;
        $confirmation->account_holder = $request->account_holder;
        $confirmation->nominal_transfer = $request->nominal_transfer;
        $confirmation->date = $request->date;
        $confirmation->note = $request->note;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'confirmations';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // save nama field image
            $confirmation->image = $filename;
        }

        $confirmation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Berhasil melakukan konfirmasi, silahkan tunggu verifikasi dari kami"
        ]);
        
        return redirect()->route('donation.index.d');
    }
}
