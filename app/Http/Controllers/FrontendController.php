<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Donation;
use App\Confirmation;
use App\Post;
use App\Page;
use App\Category;
use App\Member;
use App\Contact;
use App\Media;
use Session;

class FrontendController extends Controller
{
    public function landing()
    {
    	return view('frontends.landing');
    }

    public function donation()
    {
    	return view('frontends.donation');
    }

    public function donationStore(Request $request)
    {
    	$this->validate($request, [
            'title' => 'required|string',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|numeric',
            'address' => 'required|string',
            'nominal' => 'required|numeric',
            'category' => 'required|string',
            'bank_id' => 'required|numeric',
        ]);
        
        $donation = new Donation;
        $donation->code = Donation::generateCode();
        $donation->title = $request->title;
        $donation->name = $request->name;
        $donation->email = $request->email;
        $donation->phone = $request->phone;
        $donation->address = $request->address;
        $donation->nominal = $request->nominal;
        $donation->category = $request->category;
        $donation->bank_id = $request->bank_id;
        $donation->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Donasi Berhasil dengan Kode : $donation->code, Harap Catat Kode Donasi untuk konfirmasi, Mohon untuk melakukan konfirmasi melalui link konfirmasi yang ada di navigasi, Terima Kasih"
        ]);
        
        return redirect()->route('fe.donate');
    }

    public function confirmation()
    {
        return view('frontends.confirmation');
    }

    public function confirmationStore(Request $request)
    {
        $this->validate($request, [
          'donation_code' => 'required|string|max:8',
          'bank_id' => 'required|numeric',
          'account_holder' => 'required|string',
          'date' => 'required|date',
          'nominal_transfer' => 'required|numeric',
          'image' => 'image|max:1024',
        ]);

        $donation = Donation::where('code', $request->donation_code)->first();

        if (!$donation) {
            Session::flash("flash_notification", [
              "level"=>"danger",
              "message"=>"Maaf Kode Donasi tidak ditemukan, harap menghubungi kami untuk mendapatkan kode donasi anda."
            ]);

            return redirect()->back();
        }
        
        $confirmation = new Confirmation;
        $confirmation->donation_id = $donation->id;
        $confirmation->bank_id = $request->bank_id;
        $confirmation->account_holder = $request->account_holder;
        $confirmation->nominal_transfer = $request->nominal_transfer;
        $confirmation->date = $request->date;
        $confirmation->note = $request->note;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'confirmations';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // save nama field image
            $confirmation->image = $filename;
        }

        $confirmation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Berhasil melakukan konfirmasi, silahkan tunggu verifikasi dari kami"
        ]);
        
        return redirect()->back();
    }

    public function contact()
    {
    	return view('frontends.contact');
    }

    public function contactStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|numeric',
            'subject' => 'required|string',
            'message' => 'required|string',
        ]);
        
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Terima kasih telah menghubungi kami, silahkan tunggu balasan dari kami melalui email / sms / wa."
        ]);
        
        return view('frontends.contact');
    }

    public function galleryIndex(Request $request)
    {
        $galleries = Media::paginate(8);
        $disable = true;

        return view('frontends.gallery')->with(compact('galleries', 'disable'));
    }

    public function postIndex(Request $request)
    {
        $posts = Post::where('status', 100);

        if ($request->has('category')) {

            if ($request->category != 'all') {
                $category = Category::where('slug', $request->category)->first();

                if ($category) {
                    $posts->where('category_id', $category->id);
                }
            }

        }

        $posts = $posts->paginate(4);

        return view('frontends.post')->with(compact('posts'));
    }

    public function postShow($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post) {
            abort(404);
        }

        $post->view_count = $post->view_count + 1;
        $post->save();

        return view('frontends.post-show')->with(compact('post'));
    }

    public function member(Request $request)
    {
        $members = Member::query();

        $search = '';
        if ($request->has('search')) {
            $search = $request->search;
            $members->where('name', 'like', '%' . $search . '%');
        }

        $members = $members->paginate(100);

        return view('frontends.member')->with(compact('members', 'search')); 
    }

    public function pageShow($slug)
    {
        $page = Page::where('slug', $slug)->first();

        if (!$page) {
            abort(404);
        }

        return view('frontends.page-show')->with(compact('page'));
    }
}
