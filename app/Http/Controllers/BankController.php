<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Bank;
use Session;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $banks = Bank::query();

            $banks = $banks->select('*');
            
            return Datatables::of($banks)
                ->addIndexColumn()
                ->addColumn('action', function($bank){
                    return view('partials._action', [
                        'model'           => $bank,
                        'form_url'        => route('bank.destroy', $bank->id),
                        'edit_url'        => route('bank.edit', $bank->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('banks.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('banks.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'account_number' => 'required|numeric',
          'account_holder' => 'required|string',
          'status' => 'required|numeric',
        ]);
        
        $bank = new Bank;
        $bank->name = $request->name;
        $bank->account_number = $request->account_number;
        $bank->account_holder = $request->account_holder;
        $bank->status = $request->status;
        $bank->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Bank dengan nama $bank->name berhasil dibuat"
        ]);
        
        return redirect()->route('bank.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = Bank::find($id);

        return view('banks.edit')->with(compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'account_number' => 'required|numeric',
          'account_holder' => 'required|string',
          'status' => 'required|numeric',
        ]);
        
        $bank = Bank::find($id);
        $bank->name = $request->name;
        $bank->account_number = $request->account_number;
        $bank->account_holder = $request->account_holder;
        $bank->status = $request->status;
        $bank->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Bank dengan nama $bank->name berhasil diedit"
        ]);
        
        return redirect()->route('bank.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::find($id);

        $check = \App\Donation::where('bank_id', $id)->count();

        if ($check > 0) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat menghapus bank yang sudah memiliki data donasi!"
            ]);

            return redirect()->back();
        }

        $bank->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('bank.index');
    }
}
