<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\Category;
use App\Post;
use Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $posts = Post::with('category', 'user');

            if ($request->get('category') != null) {
                if ($request->category != 'all') {
                    $posts->where('category_id', $request->category);
                }
            }

            $posts = $posts->select('*');
            
            return Datatables::of($posts)
                ->addIndexColumn()
                ->addColumn('action', function($post){
                    return view('partials._action', [
                        'model'           => $post,
                        'form_url'        => route('post.destroy', $post->id),
                        'edit_url'        => route('post.edit', $post->id),
                        'show_url'        => route('post.show', $post->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|string|max:255',
          'content' => 'required',
          'category_id' => 'required|numeric',
          'image' => 'required|image|max:2048'
        ]);
        
        $post = new Post;
        $post->user_id = \Auth::user()->id;
        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->slug = Post::generateSlug($request->title);
        $post->content = $request->content;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'posts';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // save nama field image
            $post->image = $filename;
        }

        $post->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Artikel dengan judul $post->title berhasil dibuat"
        ]);
        
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with(compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with(compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'title' => 'required|string|max:255',
          'content' => 'required',
          'category_id' => 'required|numeric',
          'image' => 'image|max:2048'
        ]);
        
        $post = Post::find($id);
        $post->user_id = \Auth::user()->id;
        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->content = $request->content;

        if ($post->title != $request->title) {
            $post->slug = Post::generateSlug($request->title);
        }

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'posts';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($post->image) {
                $old_image = $post->image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'posts'
                 . DIRECTORY_SEPARATOR . $post->image;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $post->image = $filename;
        }

        $post->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Artikel dengan judul $post->title berhasil diedit"
        ]);
        
        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if ($post->image) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'posts'
             . DIRECTORY_SEPARATOR . $post->image;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        $post->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('post.index');
    }
}
