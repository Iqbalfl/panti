<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Category;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $categories = Category::query();

            $categories = $categories->select('*');
            
            return Datatables::of($categories)
                ->addIndexColumn()
                ->addColumn('action', function($category){
                    return view('partials._action', [
                        'model'           => $category,
                        'form_url'        => route('category.destroy', $category->id),
                        'edit_url'        => route('category.edit', $category->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'description' => 'required|string',
          'status' => 'required|numeric',
        ]);
        
        $category = new Category;
        $category->name = $request->name;
        $category->slug = Category::generateSlug($request->name);
        $category->description = $request->description;
        $category->status = $request->status;
        $category->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Kategori dengan nama $category->name berhasil dibuat"
        ]);
        
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('categories.edit')->with(compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'description' => 'required|string',
          'status' => 'required|numeric',
        ]);
        
        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->status = $request->status;

        if ($category->name != $request->name) {
            $category->slug = Category::generateSlug($request->name);
        }

        $category->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Kategori dengan nama $category->name berhasil diedit"
        ]);
        
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $check = \App\Post::where('category_id', $id)->count();

        if ($check > 0) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat menghapus kategori yang sudah memiliki data artikel!"
            ]);

            return redirect()->back();
        }

        $category->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('category.index');
    }
}
