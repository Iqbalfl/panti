<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
	protected $appends = ['display_status', 'nominal_rupiah'];
    
    public function donation()
    {
        return $this->belongsTo('App\Donation');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function getNominalRupiahAttribute()
    {
        return 'Rp '.number_format($this->attributes['nominal_transfer'],0,',','.');
    }

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Menunggu Konfirmasi";
        } else if ($value == 200) {
            $status = "Telah Diterima";
        } else if ($value == 10) {
            $status = "Dibatalkan";
        } else {
            $status = "Tidak Ada";
        }
        
        return $status;
    }
}
