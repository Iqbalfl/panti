<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $appends = ['display_status', 'display_date'];

    public function scopeGenerateSlug($query, $title)
    {
        $g_slug = str_slug($title);
        $slug_check = $this->where('slug', $g_slug)->count();
        if ($slug_check == 0) {
            $slug = $g_slug;
        } else {
            $check = 0;
            $unique = false;
            while ($unique == false) {
                $randomID = ++$check;
                $check = $this->where('slug', $g_slug.'-'.$randomID)->count();
                if ($check > 0) {
                    $unique = false;
                } else {
                    $unique = true;
                }
            }
            $slug = $g_slug.'-'.$randomID;
        }

        return $slug;
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Publish";
        } else if ($value == 10) {
            $status = "Draft";
        } else {
            $status = "Tidak Ada";
        }
        
        return $status;
    }

    public function getDisplayDateAttribute()
    {
        $value = $this->attributes['created_at'];

        $result = date('d-m-Y', strtotime($value));
        
        return $result;
    }

    public function scopeLatestPost($query, $limit)
    {
        return $query->orderBy('id', 'dsc')->limit($limit);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 100);
    }
}
