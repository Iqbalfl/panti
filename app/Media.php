<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $appends = ['media_url'];

    public function getMediaUrlAttribute()
    {
        $name = $this->name;     
        $path = $this->path;
             
        return url($path.'/'.$name);
    }
}
