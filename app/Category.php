<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $appends = ['display_status'];

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Aktif";
        } else if ($value == 10) {
            $status = "Non Aktif";
        } else {
            $status = "Tidak Ada";
        }
        
        return $status;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 100);
    }

    public function scopeGenerateSlug($query, $name)
    {
        $g_slug = str_slug($name);
        $slug_check = $this->where('slug', $g_slug)->count();
        if ($slug_check == 0) {
            $slug = $g_slug;
        } else {
            $check = 0;
            $unique = false;
            while ($unique == false) {
                $randomID = ++$check;
                $check = $this->where('slug', $g_slug.'-'.$randomID)->count();
                if ($check > 0) {
                    $unique = false;
                } else {
                    $unique = true;
                }
            }
            $slug = $g_slug.'-'.$randomID;
        }

        return $slug;
    }
}
