<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Donation extends Model
{
    protected $appends = ['display_status', 'nominal_rupiah'];

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function confirmations()
    {
        return $this->hasMany('App\Confirmation');
    }

    public function getNominalRupiahAttribute()
    {
        return 'Rp '.number_format($this->attributes['nominal'],0,',','.');
    }

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Menunggu Konfirmasi";
        } else if ($value == 200) {
            $status = "Telah Diterima";
        } else if ($value == 10) {
            $status = "Dibatalkan";
        } else {
            $status = "Tidak Ada";
        }
        
        return $status;
    }

    public function scopeGenerateCode()
    {
        $unique = false;

        while ($unique == false) {
            $code = strtoupper(substr(Str::uuid()->toString(), 0, 8));

            $check = $this->where('code',$code)->count();
            if ($check > 0) {
                $unique = false;
            } else {
                $unique = true;
            }
        }
        return $code;
    }

    public function scopeUser($query, $id)
    {
        return $query->where('user_id', $id);
    }
}
