@extends('layouts.frontend')

@section('content')

	<section class="section fs grd1">
		<div class="cl"></div>
		<div class="container">
			<div class="row">	
				<div class="col-md-6">
					<div class="welcome-message">
						<h1>Donasi</h1>
					</div><!-- end welcome -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section">
		<div class="container">
			<div class="row">

        @if (session()->has('flash_notification.message'))
        <div class="col-md-12">
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
            </div>
         </div>
        @endif

				<div class="col-md-6">
					<div class="contact-details">
            <img src="{{ asset('vendor/frontend/upload/image.jpg') }}" alt="" class="img-responsive img-thumbnail">
						<hr>
						<h4>Perhatian</h4>
						<p>Mohon kesediannya untuk melakukan konfirmasi pemberian donasi, supaya mempermudah kami dalam  rekapitulasi data administrasi</p>
					</div>
				</div>

				<div class="col-md-6">
					<form role="form" class="contactform" method="POST" action="{{ route('fe.donate.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
              <div class="form-group col-md-4 {{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title">Panggilan *</label>
                <select class="form-control" name="title">
                    <option value="Bapak">Bapak</option>
                    <option value="Ibu">Ibu</option>
                    <option value="Saudara">Saudara</option>
                    <option value="Saudari">Saudari</option>
                </select>
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group col-md-8 {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="title">Nama *</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="title">Email *</label>
			        <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
			        @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
				    </div>

				    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="title">Nomor Telepon / Whatsapp *</label>
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Telepon / WA" value="{{ old('phone') }}">
              @if ($errors->has('phone'))
                  <span class="help-block">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif
				    </div>

				    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="title">Alamat Lengkap *</label>
			        <textarea class="form-control" id="address" name="address" placeholder="Alamat Lengkap" maxlength="140" rows="7">{{ old('address') }}</textarea>
			        @if ($errors->has('address'))
                  <span class="help-block">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
              @endif
            </div>

            <div class="row">
              <div class="form-group col-md-6 {{ $errors->has('nominal') ? ' has-error' : '' }}">
                <label for="title">Jumlah Nominal *</label>
                <input type="text" class="form-control" id="name" name="nominal" placeholder="Jumlah / Nominal" value="{{ old('nominal') }}">
                @if ($errors->has('nominal'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('nominal') }}</strong>
	                  </span>
	              @endif
              </div>
              <div class="form-group col-md-6 {{ $errors->has('category') ? ' has-error' : '' }}">
                <label for="title">Kategori *</label>
                <select class="form-control" name="category">
                  <option value="Zakat">Zakat</option>
                  <option value="Infaq">Infaq</option>
                  <option value="Shodaqoh">Shodaqoh</option>
                  <option value="Waqaf">Waqaf</option>
                </select>
                @if ($errors->has('category'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('category') }}</strong>
	                  </span>
	              @endif
              </div>
            </div>

            <div class="form-group {{ $errors->has('bank_id') ? ' has-error' : '' }}">
              <label for="title">Transfer Melalui Rekening Berikut *</label> <br>
			        <select class="form-control" name="bank_id">
			        	@foreach (\App\Bank::where('status', 100)->get() as $item)
                  <option value="{{ $item->id }}">{{ $item->name.' : '.$item->account_number.'; Atas nama : '.$item->account_holder }}</option>
                @endforeach
			        </select>
			        @if ($errors->has('bank_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('bank_id') }}</strong>
                  </span>
              @endif
            </div>

				    <button type="submit" id="submit" name="submit" class="btn btn-primary">Kirim</button>
					</form>
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

@endsection