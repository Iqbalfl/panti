@extends('layouts.frontend')

@section('css')
  <style type="text/css">
    img {
      display: block;
    }
    .gallery {
      position: relative;
      z-index: 2;
      padding: 10px;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-flow: row wrap;
          flex-flow: row wrap;
      -webkit-box-pack: justify;
          -ms-flex-pack: justify;
              justify-content: space-between;
      -webkit-transition: all .5s ease-in-out;
      transition: all .5s ease-in-out;
    }
    .gallery.pop {
      -webkit-filter: blur(10px);
              filter: blur(10px);
    }
    .gallery figure {
      -ms-flex-preferred-size: 33.333%;
          flex-basis: 33.333%;
      padding: 5px;
      overflow: hidden;
      border-radius: 10px;
      cursor: pointer;
    }
    .gallery figure img {
      width: 100%;
      border-radius: 5px;
      -webkit-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
    }
    .gallery figure figcaption {
      display: none;
    }
  </style>

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
@endsection

@section('content')
	<section class="section fs grd1">
    <div class="cl"></div>
    <div class="container">
      <div class="row">	
        <div class="col-md-6">
          <div class="welcome-message">
            <h4>Halaman Informasi</h4>
            <h1>Gallery Photo</h1>
          </div><!-- end welcome -->
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- end container -->
  </section><!-- end section -->

  <section class="section">
    <div class="container">
      <div class="row">
        <div class="gallery">

        @forelse ($galleries as $image)
          <figure>
            <a href="{{ $image->media_url }}" data-toggle="lightbox">
              <img src="{{ $image->media_url }}" alt=""/>
            </a>
          </figure>
        @empty
          <h3><i>Belum ada photo</i></h3>
        @endforelse

        </div>
      </div>

      {{ $galleries->links() }}
    </div><!-- end container -->
  </section><!-- end section -->


@endsection

@section('script')
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
  <script type="text/javascript">
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
  </script>
@endsection