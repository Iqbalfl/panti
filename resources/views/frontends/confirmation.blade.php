@extends('layouts.frontend')

@section('content')

	<section class="section fs grd1">
		<div class="cl"></div>
		<div class="container">
			<div class="row">	
				<div class="col-md-6">
					<div class="welcome-message">
						<h1>Donasi</h1>
            <h4>Konfirmasi Transfer</h4>
					</div><!-- end welcome -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section">
		<div class="container">
			<div class="row">

        @if (session()->has('flash_notification.message'))
        <div class="col-md-12">
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
            </div>
         </div>
        @endif

				<div class="col-md-6 col-md-offset-3">
					<form role="form" class="contactform" method="POST" action="{{ route('fe.confirmation.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('donation_code') ? ' has-error' : '' }}">
              <label for="title">Kode Donasi *</label>
			        <input type="text" class="form-control" id="donation_code" name="donation_code" placeholder="Kode donasi contoh : D75EB700" value="{{ old('donation_code') }}">
			        @if ($errors->has('donation_code'))
                  <span class="help-block">
                      <strong>{{ $errors->first('donation_code') }}</strong>
                  </span>
              @endif
				    </div>

            <div class="form-group {{ $errors->has('bank_id') ? ' has-error' : '' }}">
              <label for="title">Transfer Melalui Rekening *</label> <br>
              <select class="form-control" name="bank_id">
                @foreach (\App\Bank::where('status', 100)->get() as $item)
                  <option value="{{ $item->id }}">{{ $item->name.' : '.$item->account_number.'; Atas nama : '.$item->account_holder }}</option>
                @endforeach
              </select>
              @if ($errors->has('bank_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('bank_id') }}</strong>
                  </span>
              @endif
            </div>

				    <div class="form-group {{ $errors->has('account_holder') ? ' has-error' : '' }}">
              <label for="title">Transfer Atas Nama</label>
              <input type="text" class="form-control" id="account_holder" name="account_holder" placeholder="Atas nama rekening" value="{{ old('account_holder') }}">
              @if ($errors->has('account_holder'))
                  <span class="help-block">
                      <strong>{{ $errors->first('account_holder') }}</strong>
                  </span>
              @endif
				    </div>

            <div class="row">
              <div class="form-group col-md-6 {{ $errors->has('nominal_transfer') ? ' has-error' : '' }}">
                <label for="title">Nominal Transfer *</label>
                <input type="text" class="form-control" id="name" name="nominal_transfer" placeholder="Nominal yang di transfer" value="{{ old('nominal_transfer') }}">
                @if ($errors->has('nominal_transfer'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('nominal_transfer') }}</strong>
	                  </span>
	              @endif
              </div>
              <div class="form-group col-md-6 {{ $errors->has('date') ? ' has-error' : '' }}">
                <label for="title">Tanggal Transfer *</label>
                <input type="date" class="form-control" id="name" name="date" placeholder="Jumlah / Nominal" value="{{ old('date') }}">
                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
              </div>
            </div>

				    <div class="form-group {{ $errors->has('note') ? ' has-error' : '' }}">
              <label for="title">Catatan</label>
			        <textarea class="form-control" id="note" name="note" placeholder="Kosongkan jika tidak ada" maxlength="140" rows="7">{{ old('note') }}</textarea>
			        @if ($errors->has('note'))
                  <span class="help-block">
                      <strong>{{ $errors->first('note') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
              <label for="title">Bukti Transfer</label>
              <input type="file" class="form-control" id="image" name="image" value="{{ old('image') }}">
              @if ($errors->has('image'))
                  <span class="help-block">
                      <strong>{{ $errors->first('image') }}</strong>
                  </span>
              @endif
            </div>

				    <button type="submit" id="submit" name="submit" class="btn btn-primary">Konfirmasi</button>
					</form>
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

@endsection