@extends('layouts.frontend')

@section('content')
  <section class="section fs grd1">
    <div class="cl"></div>
    <div class="container">
      <div class="row">	
        <div class="col-md-6">
          <div class="welcome-message">
            <h1>{{ $page->title }}</h1>
          </div><!-- end welcome -->
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- end container -->
  </section><!-- end section -->

  <section class="section">
    <div class="container">
      <div class="row">
        <div class="content col-md-8 col-md-offset-2">
          <div class="service-box">
            <i align="center" class="flaticon-medal"></i>
            <h2 align="center"> {{ $page->title }} </h4>
              <div class="elementor-widget-container">
                <p>{!! $page->content !!}</p>
              </div>
          </div><!-- end blog-desc -->
        </div><!-- end content -->
      </div><!-- end row -->
    </div><!-- end container -->
  </section><!-- end section -->
@endsection