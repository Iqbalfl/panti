@extends('layouts.frontend')

@section('content')
	
	<section class="section fs grd1">
		<div class="cl"></div>
		<div class="container">
			<div class="row">	
				<div class="col-md-6">
					<div class="welcome-message">
						<h1>Kontak Kami</h1>
					</div><!-- end welcome -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section">
		<div class="container">
			<div class="row">
				
				@if (session()->has('flash_notification.message'))
				<div class="col-md-12">
					<div class="alert alert-{{ session()->get('flash_notification.level') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{!! session()->get('flash_notification.message') !!}
						</div>
				 </div>
				@endif

				<div class="col-md-6">
					<div class="contact-details">
						<h3>Alamat e-Panti</h3>
						<p><i class="fa fa-map-marker fa-fw"></i> Jl. Ahmad Yani No.89<br>
							<i class="fa fa-fw"></i> Antapani, Kota Bandung<br>
							<i class="fa fa-fw"></i> Jawa Barat | 40112</p>
						<p><i class="fa fa-phone fa-fw"></i> Phone : (+62) 897-7878-636</p>
						<p><i class="fa fa-envelope fa-fw"></i> Email : <a href="mailto:e-panti@gmail.com">e-panti@gmail.com</a></p>

						<hr>

						<h4>Hubungi Kami</h4>
						<p>Anda bisa menghubungi kami dengan mengisi form berikut</p>
					</div>
				</div>

				<div class="col-md-6">
					<form role="form" class="contactform" method="POST" action="{{ route('fe.contact.store') }}">
						{{ csrf_field() }}

            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
			        <input type="name" class="form-control" id="name" name="name" placeholder="Nama" value="{{ old('name') }}">
			        @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
				    </div>

				    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
			        <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
			        @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
				    </div>

				    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <input type="number" class="form-control" id="phone" name="phone" placeholder="Telepon / WA" value="{{ old('phone') }}">
              @if ($errors->has('phone'))
                  <span class="help-block">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif
				    </div>
				    <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
              <input type="text" class="form-control" id="subject" name="subject" placeholder="Subjek" value="{{ old('subject') }}">
              @if ($errors->has('subject'))
                  <span class="help-block">
                      <strong>{{ $errors->first('subject') }}</strong>
                  </span>
              @endif
				    </div>
				    <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
			        <textarea class="form-control" id="message" name="message" placeholder="Pesan" maxlength="140" rows="7">{{ old('message') }}</textarea>
			        @if ($errors->has('message'))
                  <span class="help-block">
                      <strong>{{ $errors->first('message') }}</strong>
                  </span>
              @endif
            </div>
				    <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
					</form>
				</div><!-- end col -->

			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->
	
  <div id="map"></div>

@endsection

@section('script')
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAkADq7R0xf6ami9YirAM1N-yl7hdnYBhM "></script>
	<script src="{{ asset('vendor/frontend/js/map.js') }}"></script>
@endsection