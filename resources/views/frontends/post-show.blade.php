@extends('layouts.frontend')

@section('content')

	<section class="section fs grd1">
		<div class="container">
			<div class="row">	
				<div class="col-md-6">
					<div class="welcome-message">
						<h4>Halaman Informasi</h4>
						<h1>Kegiatan, Artikel dan Berita</h1>
					</div><!-- end welcome -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section">
		<div class="container">
			<div class="row">

				<div class="content col-md-8 blog-alt">
					<div class="blog-box clearfix">
						<div class="media-box">
							<img src="{{asset('uploads/images/posts/'.$post->image)}}" alt="" height="600px" class="img-responsive img-thumbnail">
						</div><!-- end media-box -->
						<div class="blog-single">
							<div class="blog-meta">
								<ul class="list-inline">
									<li><a href=""><i class="far fa-user"></i> {{ $post->user->name }}</a></li>
									<li><a href=""><i class="far fa-calendar"></i> {{ $post->display_date }}</a></li>
									<li><a href=""><i class="far fa-eye"></i> {{ $post->view_count }}</a></li>
								</ul>
							</div><!-- end meta -->
							<h3 class="post-title">{{ $post->title }}</h3>
							<p>{!! $post->content !!}</p>
						</div><!-- end blog-desc -->
					</div><!-- end blogbox -->
				</div><!-- end content -->
			
				@include('partials._sidebar')

			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

@endsection