@extends('layouts.frontend')

@section('content')
	
	<section class="section fs grd1">
		<div class="cl"></div>
		<div class="container">
			<div class="row">	
				<div class="col-md-6">
					<div class="welcome-message">
						<h4>Halaman Informasi</h4>
						<h1>Kegiatan, Artikel dan Berita</h1>
					</div><!-- end welcome -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section">
		<div class="container">
			<div class="row">
				<div class="content col-md-8">

					@forelse ($posts as $item)
						<div class="blog-box clearfix row">
							<div class="media-box col-md-4">
								<a href="{{ route('fe.post.show', $item->slug) }}" title=""><img src="{{asset('uploads/images/posts/'.$item->image)}}" alt="" height="600px" class="img-responsive img-thumbnail"></a>
							</div><!-- end media-box -->
							<div class="blog-desc col-md-8">
								<div class="blog-meta">
									<ul class="list-inline">
										<li><a href=""><i class="far fa-user"></i> {{ $item->user->name }}</a></li>
										<li><a href=""><i class="far fa-calendar"></i> {{ $item->display_date }}</a></li>
										<li><a href=""><i class="far fa-eye"></i> {{ $item->view_count }}</a></li>
									</ul>
								</div><!-- end meta -->
								<h3><a href="{{ route('fe.post.show', $item->slug) }}">{{ $item->title }}</a></h3>
								<p>{!! str_limit($item->content, 600) !!}</p>
								<a class="readmore" href="{{ route('fe.post.show', $item->slug) }}">Selengkapnya <i class="fa fa-chevron-circle-right"></i></a>
							</div><!-- end blog-desc -->
						</div><!-- end blogbox -->
					@empty
						<div class="blog-box clearfix row">
							<h3 class="text-center">Belum ada postingan :(</h3>
						</div><!-- end blogbox -->
					@endforelse



					<div class="pagination-wrapper row">
					 	{{ $posts->links() }}
					</div><!-- ne dpagi -->
				</div><!-- end content -->

        @include('partials._sidebar')

			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

@endsection