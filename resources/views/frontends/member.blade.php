@extends('layouts.frontend')

@section('content')
	
	<section class="section fs grd1">
		<div class="cl"></div>
		<div class="container">
			<div class="row">	
				<div class="col-md-6">
					<div class="welcome-message">
						<h4>Halaman Informasi</h4>
						<h1>Data Anak Panti</h1>
					</div><!-- end welcome -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section">
		<div class="container">
			<div class="row">
				<div class="content col-md-12">
					<form class="form-inline">
					  <div class="form-group">
					    <input type="text" class="form-control" name="search" placeholder="Cari nama" value="{{$search}}">
					  </div>
					  <button type="submit" class="btn btn-primary btn-xs">Cari</button>
					</form>
					<br>
					<div class="table-responsive">
	          <table class="table table-hover table-bordered table-striped datatable">
	            <thead>                                 
	              <tr>
	                <th>#</th>
	                <th>Nama</th>
	                <th>Jenis Kelamin</th>
	                <th>Umur</th>
	                <th>Jenis Masalah</th>
	                <th>Keadaan Wali</th>
	                <th>Tinggal</th>
	              </tr>
	            </thead>
	            <tbody>
	            	@forelse ($members as $key => $item)
	            		<tr>
	            			<td>{{ ++$key }}</td>
	            			<td>{{ $item->name }}</td>
	            			<td>{{ $item->gender }}</td>
	            			<td>{{ $item->age }}</td>
	            			<td>{{ $item->case_type }}</td>
	            			<td>{{ $item->parent_status }}</td>
	            			<td>{{ $item->stay_at }}</td>
	            		</tr>
	            	@empty
	            	<tr>
	            		<td colspan="7" align="center">Tidak ada data :(</td>
	            	</tr>
	            	@endforelse
	            </tbody>
	          </table>
	        </div>

        {{ $members->appends(compact('search'))->links() }}

			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

@endsection
