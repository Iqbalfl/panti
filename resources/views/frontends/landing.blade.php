@extends('layouts.frontend')

@section('content')
	<section class="section transheader homepage parallax" data-stellar-background-ratio="0.5" style="background-image:url('{{ asset('vendor/frontend/upload/bg_01.jpg') }}');">
		<div class="container">
			<div class="row">	
				<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
					<h2>Selamat Datang</h2>
					<p class="lead">Di Website {{ config('app.name') }}</p>
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section nopad">
		<div class="container-fluid">
			<div class="row text-center">
				<div class="col-md-3 col-sm-6 nopad first">
					<div class="home-service c1">
						<i class="flaticon-competition"></i>
						<p>Kami Akan Selalu Hadir Untuk Semua</p>
					</div><!-- end home-service -->
				</div><!-- end col -->

				<div class="col-md-3 col-sm-6 nopad">
					<div class="home-service c2">
						<i class="flaticon-chat"></i>
						<p>Informasi lengkap seluruh kegiatan dan donasi</p>
					</div><!-- end home-service -->
				</div><!-- end col -->

				<div class="col-md-3 col-sm-6 nopad">
					<div class="home-service c3">
						<i class="flaticon-domain"></i>
						<p>Donatur: Berdonasi semakin dimudahkan</p>
					</div><!-- end home-service -->
				</div><!-- end col -->

				<div class="col-md-3 col-sm-6 nopad last">
					<div class="home-service c4">
						<i class="flaticon-medal"></i>
						<p>Member: Dapatkan informasi langsung</p>
					</div><!-- end home-service -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->

	<section class="section overfree">
		<div class="icon-center"><i class="fa fa-heart"></i></div>
		<div class="container">
			<div class="section-title text-center">
				<h3>About Us</h3>
				<hr>
				<p class="lead">Inilah kami</p>
			</div><!-- end section-title -->

			<div class="row service-list text-center">
				<div class="col-md-4 col-sm-12 col-xs-12 first">
					<div class="service-wrapper wow fadeIn">	
						<i class="flaticon-competition"></i>
						<div class="service-details">
							<h4><a href="service-02.html" title="">Siapakah kami</a></h4>
							<p>Kami adalah panti asuhan yang akan selalu hadir untuk kita semua</p>		<br>						
						</div>
					</div><!-- end service-wrapper -->
				</div><!-- end col -->

				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="service-wrapper wow fadeIn">	
						<i class="flaticon-content"></i>
						<div class="service-details">
							<h4><a href="service-01.html" title="">Visi Kami</a></h4>
							<p>Membina dan Membangun Kepribadian Anak Asuh Yang Berakhlakul Karimah dan Mandiri.</p><br>
						</div>
					</div><!-- end service-wrapper -->
				</div><!-- end col -->

				<div class="col-md-4 col-sm-12 col-xs-12 last">
					<div class="service-wrapper wow fadeIn">	
						<i class="flaticon-html"></i>
						<div class="service-details">
							<h4><a href="service-02.html" title="">Misi kami</a></h4>
							<p>Meningkatkan Kesejahteraan dan Kemandirian Anak asuh Dalam Menyongsong Kehidupan Bermasyarakat Dimasa Depan.</p>
						</div>
					</div><!-- end service-wrapper -->
				</div><!-- end col -->
			</div><!-- end row -->

		</div><!-- end container -->
	</section><!-- end section -->
	
	<section class="section lb nopad spotlight style1">
	    <div class="image col-md-4 hidden-sm hidden-xs">
	        <img src="{{ asset('vendor/frontend/upload/blog_alt_01.jpg') }}" alt="" />
	    </div>
	    <div class="content">
	        <h2>Berbagi Kebahagiaan</h2>
	        <p>Anak-anak yang tinggal di Panti Asuhan kami membutuhkan banyak makanan, minuman, pakaian, kesehatan, dan pendidikan. Dukungan anda akan mengubah nasib mereka. Kami benar-benar tergantung pada anda. Tidak satupun pencapaian kami terwujud tanpa dukungan dari donatur, sponsor, dan individu yang baik hati semuanya. Dukungan anda vital untuk kami. Kami harap para donatur tetap membantu kami. Jangan ragu untuk memberi kami, kecil – besar kami tetap menghargai. Terima Kasih. </p>
	         <a href="{{ route('fe.donate') }}" class="btn btn-transparent">Donasi Sekarang</a>
	    </div>
	</section>

	<section class="section bt">
		<div class="container">
			<div class="section-title text-center">
				<small></small>
				<h3>Informasi Terbaru</h3>
			</div><!-- end section-title -->

			<div class="owl-carousel owl-theme lightcasestudies withhover">

				@foreach (\App\Post::latestPost(4)->get() as $post)
				<div class="item-carousel">
					<div class="case-box">
						<img src="{{ asset('uploads/images/posts/'.$post->image )}}" alt="" class="img-responsive">
						<div class="magnifier">
							<a href="{{ route('fe.post.show', $post->slug) }}"><i class="fa fa-link"></i></a> 
						</div>
					</div><!-- end case-box -->
				</div><!-- end col -->
				@endforeach
				
			</div>
	    </div>
	</section>
@endsection