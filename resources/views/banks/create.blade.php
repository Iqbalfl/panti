@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Bank</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tambah Bank</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('bank.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Bank</label>
              <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ old('name') }}">
              @if ($errors->has('name'))
                <div class="invalid-feedback">
                  {{ $errors->first('name') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('account_number') ? ' has-error' : '' }}">
              <label for="account_number">Nomor Rekening</label>
              <input id="account_number" type="number" class="form-control @if ($errors->has('account_number')) is-invalid @endif" name="account_number" tabindex="1" value="{{ old('account_number') }}">
              @if ($errors->has('account_number'))
                <div class="invalid-feedback">
                  {{ $errors->first('account_number') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('account_holder') ? ' has-error' : '' }}">
              <label for="account_holder">Atas Nama Rekening</label>
              <input id="account_holder" type="text" class="form-control @if ($errors->has('account_holder')) is-invalid @endif" name="account_holder" tabindex="1" value="{{ old('account_holder') }}">
              @if ($errors->has('account_holder'))
                <div class="invalid-feedback">
                  {{ $errors->first('account_holder') }}
                </div>
              @endif
            </div>
         
            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="status">Status</label>
              <select name="status" class="form-control">
                <option value="100">Aktif</option>
                <option value="0">Non Aktif</option>
              </select>
              @if ($errors->has('status'))
                <div class="invalid-feedback">
                  {{ $errors->first('status') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection
