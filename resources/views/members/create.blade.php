@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Anak Panti</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tambah Anak Panti</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('member.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Lengkap</label>
              <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ old('name') }}">
              @if ($errors->has('name'))
                <div class="invalid-feedback">
                  {{ $errors->first('name') }}
                </div>
              @endif
            </div>
            
            <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
              <label for="gender">Jenis Kelamin</label>
              <select name="gender" class="form-control">
                <option>Laki-laki</option>
                <option>Perempuan</option>
              </select>
              @if ($errors->has('gender'))
                <div class="invalid-feedback">
                  {{ $errors->first('gender') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('age') ? ' has-error' : '' }}">
              <label for="age">Umur</label>
              <input id="age" type="text" class="form-control @if ($errors->has('age')) is-invalid @endif" name="age" tabindex="1" value="{{ old('age') }}">
              @if ($errors->has('age'))
                <div class="invalid-feedback">
                  {{ $errors->first('age') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('case_type') ? ' has-error' : '' }}">
              <label for="case_type">Jenis Malalah</label>
              <input id="case_type" type="text" class="form-control @if ($errors->has('case_type')) is-invalid @endif" name="case_type" tabindex="1" value="{{ old('case_type') }}">
              @if ($errors->has('case_type'))
                <div class="invalid-feedback">
                  {{ $errors->first('case_type') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('parent_status') ? ' has-error' : '' }}">
              <label for="parent_status">Keadaan Wali</label>
              <input id="parent_status" type="text" class="form-control @if ($errors->has('parent_status')) is-invalid @endif" name="parent_status" tabindex="1" value="{{ old('parent_status') }}">
              @if ($errors->has('parent_status'))
                <div class="invalid-feedback">
                  {{ $errors->first('parent_status') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('stay_at') ? ' has-error' : '' }}">
              <label for="stay_at">Tinggal</label>
              <input id="stay_at" type="text" class="form-control @if ($errors->has('stay_at')) is-invalid @endif" name="stay_at" tabindex="1" value="{{ old('stay_at') }}">
              @if ($errors->has('stay_at'))
                <div class="invalid-feedback">
                  {{ $errors->first('stay_at') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection
