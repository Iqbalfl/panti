@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-start mb-4">
    <h1 class="h3 mb-0 text-gray-800">Artikel</h1>
    <a class="btn btn-sm btn-primary mr-auto ml-4" href="{{ route('post.create') }}"><i class="fa fa-plus"></i> Tambah Artikel</a> 
    <div class="form-inline">
      <label for="filter">Filter Kategori</label>
      <select name="category" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        @foreach (\App\Category::all() as $item)
          <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
      </select>
      <button class="btn btn-sm btn-primary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Artikel</h6>
        </div>
        <div class="card-body">

        <div class="table-responsive">
          <table class="table table-hover table-bordered datatable">
            <thead>                                 
              <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Penulis</th>
                <th>Tanggal Publish</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('vendor/sb-temp/vendor/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('post.index') }}',
            data: function (d) {
              d.category = $('select[name=category]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'title', name: 'title'},
            {data: 'category.name', name: 'category.name'},
            {data: 'user.name', name: 'user.name'},
            {data: 'display_date', name: 'display_date', orderable: false, searchable: false},
            {data: 'display_status', name: 'display_status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });
</script>
@endsection
