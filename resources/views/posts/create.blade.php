@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Artikel</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tambah Artikel</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
              <label for="title">Judul Artikel</label>
              <input id="title" type="text" class="form-control @if ($errors->has('title')) is-invalid @endif" name="title" tabindex="1" value="{{ old('title') }}">
              @if ($errors->has('title'))
                <div class="invalid-feedback">
                  {{ $errors->first('title') }}
                </div>
              @endif
            </div>

            <label for="image">Gambar</label>
            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} custom-file mb-3">
              <input id="image" type="file" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" name="image" tabindex="1">
              <label class="custom-file-label" for="customFile">Pilih Gambar</label>
              @if ($errors->has('image'))
                <div class="invalid-feedback">
                  {{ $errors->first('image') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
              <label for="category_id">Kategori</label>
              <select name="category_id" class="form-control">
                @foreach (\App\Category::all() as $item)
                  <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('category_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('category_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
              <label for="content">Konten</label>
              <textarea class="form-control @if ($errors->has('content')) is-invalid @endif" name="content" tabindex="1">{{ old('content') }}</textarea>
              @if ($errors->has('content'))
                <div class="invalid-feedback">
                  {{ $errors->first('content') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('textarea').trumbowyg({
        lang: 'id'
    });
  </script>
@endsection