@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Artikel</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Lihat Artikel</h6>
        </div>
        <div class="card-body">
          
          <p>
            Judul: <br><strong>{{$post->title}}</strong>
            <hr>
          </p>
          <p>
            Penulis: <br><strong>{{$post->user->name}}</strong>
            <hr>
          </p>
          <p>
            Kategori: <br><strong>{{$post->category->name}}</strong>
            <hr>
          </p>
          <p>
            Tanggal Publish: <br><strong>{{$post->display_date}}</strong>
            <hr>
          </p>
          <p>
            Gambar: <br><img src="{{asset('uploads/images/posts/'.$post->image)}}" class="rounded d-block" alt="image" style="max-width: 500px;">
            <hr>
          </p>
          <p>
            Isi Konten: <br>{!! $post->content !!}
          </p>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('textarea').trumbowyg({
        lang: 'id'
    });
  </script>
@endsection