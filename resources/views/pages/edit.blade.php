@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Halaman</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Edit Halaman</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('page.update', $page->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
              <label for="title">Judul Halaman</label>
              <input id="title" type="text" class="form-control @if ($errors->has('title')) is-invalid @endif" name="title" tabindex="1" value="{{ $page->title }}">
              @if ($errors->has('title'))
                <div class="invalid-feedback">
                  {{ $errors->first('title') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
              <label for="content">Konten</label>
              <textarea class="form-control @if ($errors->has('content')) is-invalid @endif" name="content" tabindex="1">{!! $page->content !!}</textarea>
              @if ($errors->has('content'))
                <div class="invalid-feedback">
                  {{ $errors->first('content') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('textarea').trumbowyg({
        lang: 'id'
    });
  </script>
@endsection