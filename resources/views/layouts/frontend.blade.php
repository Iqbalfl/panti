<!DOCTYPE html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <!-- Mobile Meta -->
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  <!-- Site Meta -->
  <title>{{ config('app.name') }}</title>
  <meta name="keywords" content="{{ config('app.name') }}">
  <meta name="description" content="">
  <meta name="author" content="">
  
  <!-- Site Icons -->
  <link rel="shortcut icon" href="{{ asset('vendor/frontend/images/favicon.png') }}" type="image/x-icon" />

	<!-- Google Fonts -->
 	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700" rel="stylesheet"> 

	<!-- Custom & Default Styles -->
	<link rel="stylesheet" href="{{ asset('vendor/frontend/modules/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/frontend/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/frontend/css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/frontend/css/carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/frontend/style.css') }}">

	@yield('css')

</head>
<body>
	
	<div id="wrapper">

		<header class="header site-header">
			<div class="container">
				<nav class="navbar navbar-default yamm">
			    <div class="container-fluid">
		        <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
	            </button>
						<a class="navbar-brand" href="{{ url('/') }}">
							<img src="{{ asset('vendor/frontend/images/logo.png') }}" alt="Logo" style="max-height: 50px;">
						</a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
	            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/') }}">Beranda</a></li>
                <li><a href="{{ route('fe.post') }}">Kegiatan</a></li>
                <li><a href="{{ route('fe.gallery') }}">Gallery</a></li>
								<li><a href="{{ route('fe.contact') }}">Kontak</a></li>
								<li class="dropdown yamm-fw hasmenu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="fa fa-angle-down"></span></a>
									<ul class="dropdown-menu">
										@foreach (\App\Page::active()->get() as $item)
											<li><a href="{{ route('fe.page.show', $item->slug) }}">{{ $item->title }}</a></li>
										@endforeach
									</ul>
								</li>
								<li class="hidden-md hidden-lg"><a href="{{ route('fe.donate') }}"><i class="fas fa-donate"></i> Donasi</a></li>
								<li class="lastlink hidden-xs hidden-sm"><a class="btn btn-primary" href="{{ route('fe.donate') }}"><i class="fas fa-donate"></i> DONASI</a></li>
							</ul>
		        </div><!--/.nav-collapse -->
			    </div><!--/.container-fluid -->
				</nav><!-- end nav -->
			</div><!-- end container -->
		</header><!-- end header -->

		@yield('content')

		<footer class="footer primary-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="widget clearfix">
							<h4 class="widget-title">Berlangganan email</h4>
							<div class="newsletter-widget">
								<p>Dapatkan informasi terbaru dari kami langsung ke email anda</p>
									<form class="form-inline" role="search">
										<div class="form-1">
											<input type="text" class="form-control" placeholder="Masukan email disini..">
											<button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i></button>
										</div>
									</form>
							</div><!-- end newsletter -->
						</div><!-- end widget -->
					</div><!-- end col -->
					<div class="col-md-2 col-sm-2">
						<div class="widget clearfix">
							<h4 class="widget-title">Menu</h4>
							<ul>
								<li><a href="{{ route('fe.member') }}">Data Anak Panti</a></li>
								<li><a href="{{ route('fe.post') }}">Kegiatan</a></li>
								<li><a href="{{ route('fe.gallery') }}">Gallery</a></li>
								<li><a href="{{ route('fe.contact') }}">Kontak</a></li>
							</ul>
						</div><!-- end widget -->
					</div><!-- end col -->

					<div class="col-md-2 col-sm-2">
						<div class="widget clearfix">
							<h4 class="widget-title">Navigasi</h4>
							<ul>
								<li><a href="{{ route('fe.confirmation') }}">Konfirmasi Transfer</a></li>
								<li><a href="{{ route('fe.donate') }}">Donasi</a></li>
								<li><a href="{{ route('login') }}">Login</a></li>
								<li><a href="{{ route('register') }}">Daftar</a></li>
						</div><!-- end widget -->
					</div><!-- end col -->

					<div class="col-md-2 col-sm-2">
						<div class="widget clearfix">
							<h4 class="widget-title">Sosial media</h4>
							<ul>
								<li><a href="https://facebook.com/">Facebook</a></li>
								<li><a href="https://twitter.com/">Twitter</a></li>
								<li><a href="https://youtube.com/">Youtube</a></li>
								<li><a href="https://instagram.com/">Instagram</a></li>
							</ul>
						</div><!-- end widget -->
          </div><!-- end col -->
            
				</div><!-- end row -->
			</div><!-- end container -->
		</footer><!-- end primary-footer -->

		<footer class="footer secondary-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<p>2019 &copy; <a href="#">{{ config('app.name') }}</a>. All rights reserved.</p>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="list-inline">
							<li class="hidden">Designed: <a href="https://html.design">HTML Design</a></li>
						</ul>
					</div><!-- end col -->
				</div><!-- end row -->
			</div><!-- end container -->
		</footer><!-- end second footer -->
	</div><!-- end wrapper -->

	<!-- jQuery Files -->
	<script src="{{ asset('vendor/frontend/js/jquery.min.js') }}"></script>
	<script src="{{ asset('vendor/frontend/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendor/frontend/js/parallax.js') }}"></script>
	<script src="{{ asset('vendor/frontend/js/animate.js') }}"></script>
	<script src="{{ asset('vendor/frontend/js/owl.carousel.js') }}"></script>

	@if (!isset($disable))
		<script src="{{ asset('vendor/frontend/js/custom.js') }}"></script>
	@endif

	@yield('script')
	
</body>
</html>