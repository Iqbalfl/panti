@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Pesan Masuk</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Lihat Pesan</h6>
        </div>
        <div class="card-body">
          
          <p>
            Nama : <br><strong>{{$contact->name}}</strong>
            <hr>
          </p>          
          <p>
            Email : <br><strong>{{$contact->email}}</strong>
            <hr>
          </p>          
          <p>
            Telepon : <br><strong>{{$contact->phone}}</strong>
            <hr>
          </p>          
          <p>
            Subject : <br><strong>{{$contact->subject}}</strong>
            <hr>
          </p>
          <p>
            Isi Pesan : <br>{!! $contact->message !!}
          </p>

        </div>
      </div>
    </div>
  </div>
@endsection