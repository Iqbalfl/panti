@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1> 
    <div>Ringkasan Donasi : {{Auth::user()->title.' '.Auth::user()->name}}</div> 
  </div>

  <!-- Content Row -->
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-6 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Donasi Telah Diterima</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">Rp {{ $data['success'] }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-donate fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-6 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Donasi Belum diKonfirmasi</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Rp {{ $data['pending'] }}</div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-donate fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Selamat datang</h6>
        </div>
        <div class="card-body">
          
          @if (session('status'))
              <div class="alert alert-primary" role="alert">
                  {{ session('status') }}
              </div>
          @endif
          <i>{{Auth::user()->title.' '.Auth::user()->name}}</i>
        
        </div>
      </div>
    </div>
  </div>
@endsection
