@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-start mb-4">
    <h1 class="h3 mb-0 text-gray-800 mr-auto">Konfirmasi Transfer</h1>

    <form class="form-inline">
      <label for="date_range">Filter Range Tanggal</label>
      <input type="text" id="date_range" name="date" value="{{ $date }}" class="form-control-sm ml-2 mr-2">

      <label for="status">Filter Status</label>
      <select name="status" class="form-control-sm ml-2">
        @foreach (config('setting.confirmation_status') as $key => $item)
          <option value="{{$item['number']}}" @if ($item['number'] == $status) selected @endif>{{$item['string']}}</option>
        @endforeach
      </select>
      <button class="btn btn-sm btn-primary ml-2" id="btn-filter"><i class="fas fa-filter"></i> Filter</button>
    </form>
  </div>

  <div class="row">
    
    @forelse ($confirmations as $item)
      <div class="col-md-6">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <strong class="text-dark">#{{$item->donation->code}} -</strong> 
            @if ($item->status == 200)
              <strong class="text-info">{{ strtoupper($item->display_status)}}</strong>
            @elseif($item->status == 10)
              <strong class="text-danger">{{ strtoupper($item->display_status)}}</strong>
            @else
              <strong class="text-warning">{{ strtoupper($item->display_status)}}</strong>
            @endif
          </div>
          <div class="card-body">

            <div class="row">
              <div class="col-md-6">
                <p>
                  Melalui Bank: <br><strong class="text-info">{{$item->bank->name}}</strong>
                </p>
              </div>
              <div class="col-md-6">
                <p>
                  Atas Nama: <br><strong class="text-info">{{$item->account_holder}}</strong>
                </p>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <p>
                  Nominal Donasi: <br><strong class="text-info">{{$item->donation->nominal_rupiah}}</strong>
                </p>
              </div>
              <div class="col-md-6">
                <p>
                  Nominal Transfer: <br><strong class="text-info">{{$item->nominal_rupiah}}</strong>
                </p>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <p>
                  Tanggal Transfer: <br><strong class="text-info">{{ date('d-m-Y', strtotime($item->date))}}</strong>
                </p>
              </div>
              <div class="col-md-6">
                <p>
                  Catatan: <br><strong class="text-info">{{$item->note}}</strong>
                </p>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <p>
                  Bukti Transfer: <br>
                  <a href="{{asset('uploads/images/confirmations/'.$item->image)}}" target="_blank" class="btn btn-sm btn-warning text-white"><i class="fas fa-image"></i> Lihat</a>
                </p>
              </div>
              <div class="col-md-6">
              </div>
            </div>
            
          </div>
          <div class="card-footer text-center">
            <form action="{{ route('confirmation.approve', $item->id) }}" method="post">
                {{csrf_field()}} 
                {{method_field('patch')}}

                <a href="{{ route('donation.show', $item->donation_id) }}" class="btn btn-sm btn-info text-white"><i class="fas fa-info"></i> Info</a>
                
                <button type="submit" value="Delete" class="btn btn-sm btn-success text-white js-submit-confirm" title="Terima Pembayaran" @if($item->status == 200 || $item->status == 10) disabled @endif>
                    <i class="fas fa-check"></i> Terima
                </button>
                
                <a href="{{ route('confirmation.cancel', $item->id) }}" class="btn btn-sm btn-danger text-white cancel-confirm"><i class="fas fa-times"></i> Batalkan</a>
            </form>
            
          </div>
        </div>
      </div>
    @empty
      <div class="col-md-12">
        <div class="card shadow mb-4">
          <div class="card-body text-center">
            <b>Tidak Ada Data Konfirmasi</b>
          </div>
        </div>
      </div>
    @endforelse
    
  </div>

  <div class="row">
    <div class="col-md-5">
      {{ $confirmations->appends(compact('date', 'status'))->links() }}
    </div>
    <div class="col-md-7 text-right">
      <div class="pagination-info mb-2">
          Menampilkan hasil <span class="pagination-info-from">{{$confirmations->firstItem() ?? 0}}</span> - <span class="pagination-info-to">{{$confirmations->lastItem() ?? 0}}</span> dari <span class="pagination-info-total">{{$confirmations->total()}} entri</span>
      </div>
    </div>
  </div>

@endsection
@section('script')
<script>
  $(document).on('click','.js-submit-confirm', function(e){
      e.preventDefault();
      swal({
        title: 'Apakah anda yakin ingin menerima pembayaran ini?',
        text: 'Pastikan data konfirmasi sudah di cek dan valid!',
        icon: 'info',
        buttons: true,
        dangerMode: false,
      })
      .then((willNext) => {
        if (willNext) {
          $(this).closest('form').submit();
        } 
      });
  });

  $(document).on('click','.cancel-confirm', function(e){
      e.preventDefault();
      var link = $(this).attr('href');
      swal({
        title: 'Apakah anda yakin ingin membatalkan pembayaran ini?',
        text: 'Pastikan data konfirmasi sudah di cek dan valid!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willNext) => {
        if (willNext) {
          window.location.href = link;
        } 
      });
  });

  $(function() {
    $('#date_range').daterangepicker({
      opens: 'left',
    }, function(start, end, label) {
      console.log("Date result : " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
  });

</script>
@endsection
