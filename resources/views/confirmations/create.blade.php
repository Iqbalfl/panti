@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Konfirmasi Transfer</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Buat Konfirmasi Transfer</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('confirmation.store.d') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

              <div class="form-group {{ $errors->has('donation_code') ? ' has-error' : '' }}">
                <label for="donation_code">Kode Donasi</label>
                <input id="donation_code" type="text" class="form-control @if ($errors->has('donation_code')) is-invalid @endif" name="donation_code" tabindex="1" value="#{{ $donation->code }}" readonly="">
                <input type="hidden" name="donation_id" value="{{ $donation->id }}">
                @if ($errors->has('donation_code'))
                  <div class="invalid-feedback">
                    {{ $errors->first('donation_code') }}
                  </div>
                @endif
              </div>

              <div class="form-group {{ $errors->has('nominal') ? ' has-error' : '' }}">
                <label for="nominal">Nominal Donasi</label>
                <input id="nominal" type="text" class="form-control @if ($errors->has('nominal')) is-invalid @endif" name="nominal" tabindex="1" value="{{ $donation->nominal_rupiah }}" readonly="">
                @if ($errors->has('nominal'))
                  <div class="invalid-feedback">
                    {{ $errors->first('nominal') }}
                  </div>
                @endif
              </div>

              <div class="form-group {{ $errors->has('bank_id') ? ' has-error' : '' }}">
                <label for="bank_id">Transfer melalui bank</label>
                <select  name="bank_id" class="form-control @if ($errors->has('bank_id')) is-invalid @endif" tabindex="1">
                  @foreach (\App\Bank::where('status', 100)->get() as $item)
                    <option value="{{ $item->id }}">{{ $item->name.' : '.$item->account_number.'; AN. '.$item->account_holder }}</option>
                  @endforeach
                </select>
                @if ($errors->has('bank_id'))
                  <div class="invalid-feedback">
                    {{ $errors->first('nominal') }}
                  </div>
                @endif
              </div>

              <div class="form-group {{ $errors->has('account_holder') ? ' has-error' : '' }}">
                <label for="account_holder">Transfer Atas Nama</label>
                <input id="account_holder" type="text" class="form-control @if ($errors->has('account_holder')) is-invalid @endif" name="account_holder" tabindex="1" value="{{ old('account_holder') }}">
                @if ($errors->has('account_holder'))
                  <div class="invalid-feedback">
                    {{ $errors->first('account_holder') }}
                  </div>
                @endif
              </div>

              <div class="form-group {{ $errors->has('nominal_transfer') ? ' has-error' : '' }}">
                <label for="nominal_transfer">Nominal Transfer</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Rp</span>
                  </div>
                  <input type="number" class="form-control  @if ($errors->has('nominal_transfer')) is-invalid @endif" name="nominal_transfer" aria-label="nominal_transfer" aria-describedby="basic-addon1" tabindex="1">
                  @if ($errors->has('nominal_transfer'))
                    <div class="invalid-feedback">
                      {{ $errors->first('nominal_transfer') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                <label for="date">Tanggal Transfer</label>
                <input id="date" type="date" class="form-control @if ($errors->has('date')) is-invalid @endif" name="date" tabindex="1" value="{{ old('date') }}">
                @if ($errors->has('date'))
                  <div class="invalid-feedback">
                    {{ $errors->first('date') }}
                  </div>
                @endif
              </div>

              <div class="form-group {{ $errors->has('note') ? ' has-error' : '' }}">
                <label for="note">Catatan</label>
                <textarea  id="note" type="text" class="form-control @if ($errors->has('note')) is-invalid @endif" name="note" tabindex="1" placeholder="Kosongkan jika tidak ada">{{ old('note') }}</textarea>
                @if ($errors->has('note'))
                  <div class="invalid-feedback">
                    {{ $errors->first('note') }}
                  </div>
                @endif
              </div>

              <label for="image">Bukti Transfer</label>
              <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} custom-file mb-3">
                <input id="image" type="file" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" name="image" tabindex="1">
                <label class="custom-file-label" for="customFile">Pilih Foto</label>
                @if ($errors->has('image'))
                  <div class="invalid-feedback">
                    {{ $errors->first('image') }}
                  </div>
                @endif
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                  Simpan
                </button>
              </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection
