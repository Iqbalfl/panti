@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Profile</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Edit Profile</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('profile.update', $user->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('patch') }}

            <div class="row">
              <div class="form-group col-6 {{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title">Panggilan</label>
                <select  name="title" class="form-control @if ($errors->has('title')) is-invalid @endif" tabindex="1">
                    <option @if ($user->title == "Bapak") selected @endif>Bapak</option>
                    <option @if ($user->title == "Ibu") selected @endif>Ibu</option>
                    <option @if ($user->title == "Saudara") selected @endif>Saudara</option>
                    <option @if ($user->title == "Saudari") selected @endif>Saudari</option>
                </select>
                @if ($errors->has('title'))
                  <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                  </div>
                @endif
              </div>
              <div class="form-group col-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Nama Lengkap</label>
                <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $user->name }}">
                @if ($errors->has('name'))
                  <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                  </div>
                @endif
              </div>
            </div>
            
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" tabindex="1" value="{{ $user->email }}">
              @if ($errors->has('email'))
                <div class="invalid-feedback">
                  {{ $errors->first('email') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="phone">Nomor Telepon</label>
              <input id="phone" type="text" class="form-control @if ($errors->has('phone')) is-invalid @endif" name="phone" tabindex="1" value="{{ $user->phone }}">
              @if ($errors->has('phone'))
                <div class="invalid-feedback">
                  {{ $errors->first('phone') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="address">Alamat</label>
              <textarea id="address" type="text" class="form-control @if ($errors->has('address')) is-invalid @endif" name="address" tabindex="1">{{ $user->address }}</textarea>
              @if ($errors->has('address'))
                <div class="invalid-feedback">
                  {{ $errors->first('address') }}
                </div>
              @endif
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="avatar">Avatar</label>
              @if (is_null($user->avatar))
                <img src="{{asset('vendor/sb-temp/img/avatar-1.jpg')}}" class="rounded d-block" width="200" alt="avatar">
              @else
                <img src="{{asset('uploads/images/avatars/'.$user->avatar)}}" class="rounded d-block" width="200" alt="avatar">
              @endif
              <br>
              <i class="small">** Maximum size gambar adalah 1MB</i><br>
              <i class="small">** Disarankan upload gambar dengan rasio 1:1 / persegi</i>
            </div>

            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }} custom-file mb-3">
              <input id="avatar" type="file" class="custom-file-input @if ($errors->has('avatar')) is-invalid @endif" name="avatar" tabindex="1">
              <label class="custom-file-label" for="customFile">Pilih Gambar</label>
              @if ($errors->has('avatar'))
                <div class="invalid-feedback">
                  {{ $errors->first('avatar') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection
