@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-start mb-4">
    <h1 class="h3 mb-0 text-gray-800">Pengguna</h1>
    <a class="btn btn-sm btn-primary mr-auto ml-4" href="{{ route('user.create') }}"><i class="fa fa-plus"></i> Buat Akun</a> 
    <div class="form-inline">
      <label for="email">Filter Role</label>
      <select name="role" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        @foreach (\App\Role::all() as $item)
          <option value="{{$item->name}}">{{$item->display_name}}</option>
        @endforeach
      </select>
      <button class="btn btn-sm btn-primary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Pengguna</h6>
        </div>
        <div class="card-body">

        <div class="table-responsive">
          <table class="table table-hover table-bordered datatable">
            <thead>                                 
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Telepon</th>
                <th>Role</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('vendor/sb-temp/vendor/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('user.index') }}',
            data: function (d) {
              d.role = $('select[name=role]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'role_name', name: 'role.name', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });
</script>
@endsection
