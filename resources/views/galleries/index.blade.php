@extends('layouts.main')

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/sb-temp/vendor/dropzonejs/dropzone.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/sb-temp/vendor/chocolat/dist/css/chocolat.css') }}">
@endsection

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Gallery</h1> 
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Upload</h6>
        </div>
        <div class="card-body">
          <form method="post" action="{{ route('dropzone.store' )}}" enctype="multipart/form-data" class="dropzone dz-clickable" id="dropzone">
            {{ csrf_field() }}
            <div class="dz-message">
              <div class="col-xs-8">
                <div class="message">
                  <p>Tarik photo kesini atau klik disini untuk upload</p>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="card-footer">
          <button type="button" class="btn btn-sm btn-primary float-right" onclick="location.reload();"><i class="fa fa-redo"></i> Save & Refresh</button>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Photo</h6>
        </div>
        <div class="card-body">
          <div class="gallery gallery-lg">
            @forelse ($galleries as $image)
              <div class="gallery-item" data-image="{{ $image->media_url }}" data-title="{{ $image->name }}">
                <span class="gallery-delete" id="{{$image->id}}" title="Hapus">&times;</span>
              </div>
            @empty
              <div class="text-center">Belum ada photo :(</div>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('vendor/sb-temp/vendor/chocolat/dist/js/jquery.chocolat.min.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/dropzonejs/dropzone.js') }} "></script>

  <script type="text/javascript">
    Dropzone.options.dropzone =
    {
      maxFilesize: 5,
      renameFile: function (file) {
        var dt = new Date();
        var time = dt.getTime();
        return time + file.name;
      },
      acceptedFiles: ".jpeg,.jpg,.png,.gif",
      addRemoveLinks: true,
      dictRemoveFile: 'Hapus',
      timeout: 50000,
      removedfile: function (file) {
        var name = file.upload.filename;
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          },
          type: 'POST',
          url: '{{ route('dropzone.destroy') }}',
          data: {_method: 'delete', filename: name},
          success: function (data) {
              console.log(data);
          },
          error: function (e) {
              console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 0;
      },

      success: function (file, response) {
        console.log(response);
      },
      error: function (file, response) {
        return false;
      }
    };
  </script>

  <script type="text/javascript">
    $(".gallery .gallery-item").each(function() {
      var me = $(this);

      me.attr('href', me.data('image'));
      me.attr('title', me.data('title'));
      if(me.parent().hasClass('gallery-fw')) {
        me.css({
          height: me.parent().data('item-height'),
        });
        me.find('div').css({
          lineHeight: me.parent().data('item-height') + 'px'
        });
      }
      me.css({
        backgroundImage: 'url("'+ me.data('image') +'")'
      });
    });
    if(jQuery().Chocolat) { 
      $(".gallery").Chocolat({
        className: 'gallery',
        imageSelector: '.gallery-item',
      });
    }

    $(document).on('click','.gallery-delete', function(e){
        e.preventDefault();
        swal({
          title: 'Apakah gambar yang dipilih akan dihapus?',
          icon: 'warning',
          buttons: true,
          dangerMode: true,
        })
        .then((willNext) => {
          if (willNext) {
            $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              },
              type: 'POST',
              url: '{{ route('gallery.destroy') }}',
              data: {_method: 'delete', media_id: this.id},
              success: function (data) {
                  console.log(data.message);
                  if (data.message == 'success'){
                    location.reload();
                  }
              },
              error: function (e) {
                  console.log(e);
              }
            });
          } 
        });
    });
  </script>
@endsection
