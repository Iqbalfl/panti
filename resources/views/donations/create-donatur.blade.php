@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Donasi</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tambah Donasi</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('donation.store.d') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
              <div class="form-group col-6 {{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="name">Panggilan</label>
                <input id="title" type="text" class="form-control @if ($errors->has('title')) is-invalid @endif" name="title" tabindex="1" value="{{ $user->title }}" readonly="">
                @if ($errors->has('title'))
                  <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                  </div>
                @endif
              </div>

              <div class="form-group col-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Nama Lengkap</label>
                <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $user->name }}" readonly="">
                @if ($errors->has('name'))
                  <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                  </div>
                @endif
              </div>
            </div>
            
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" tabindex="1" value="{{ $user->email }}" readonly="">
              @if ($errors->has('email'))
                <div class="invalid-feedback">
                  {{ $errors->first('email') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="phone">Nomor Telepon</label>
              <input id="phone" type="text" class="form-control @if ($errors->has('phone')) is-invalid @endif" name="phone" tabindex="1" value="{{ $user->phone }}" readonly="">
              @if ($errors->has('phone'))
                <div class="invalid-feedback">
                  {{ $errors->first('phone') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="address">Alamat</label>
              <textarea  id="address" type="text" class="form-control @if ($errors->has('address')) is-invalid @endif" name="address" tabindex="1" readonly="">{{ $user->address }}</textarea>
              @if ($errors->has('address'))
                <div class="invalid-feedback">
                  {{ $errors->first('address') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('nominal') ? ' has-error' : '' }}">
              <label for="nominal">Nominal</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Rp</span>
                </div>
                <input type="number" class="form-control  @if ($errors->has('nominal')) is-invalid @endif" name="nominal" aria-label="nominal" aria-describedby="basic-addon1">
                @if ($errors->has('nominal'))
                  <div class="invalid-feedback">
                    {{ $errors->first('nominal') }}
                  </div>
                @endif
              </div>
            </div>

            <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
              <label for="category">Kategori</label>
              <select name="category" class="form-control @if ($errors->has('category')) is-invalid @endif" tabindex="1">
                <option>Zakat</option>
                <option>Infaq</option>
                <option>Shodaqoh</option>
                <option>Waqaf</option>
              </select>
              @if ($errors->has('category'))
                <div class="invalid-feedback">
                  {{ $errors->first('category') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('bank_id') ? ' has-error' : '' }}">
              <label for="bank_id">Transfer melalui bank</label>
              <select  name="bank_id" class="form-control @if ($errors->has('bank_id')) is-invalid @endif" tabindex="1">
                @foreach (\App\Bank::where('status', 100)->get() as $item)
                  <option value="{{ $item->id }}">{{ $item->name.' : '.$item->account_number.'; AN. '.$item->account_holder }}</option>
                @endforeach
              </select>
              @if ($errors->has('bank_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('nominal') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection
