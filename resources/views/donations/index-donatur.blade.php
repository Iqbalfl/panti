@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-start mb-4">
    <h1 class="h3 mb-0 text-gray-800">Donasi</h1>
    <a class="btn btn-sm btn-primary mr-auto ml-4" href="{{ route('donation.create.d') }}"><i class="fa fa-plus"></i> Tambah Donasi</a> 
    <div class="form-inline">
      <label for="email">Filter Status</label>
      <select name="status" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        @foreach (config('setting.donation_status') as $key => $item)
          <option value="{{$key}}">{{$item}}</option>
        @endforeach
      </select>
      <button class="btn btn-sm btn-primary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
    </div>
  </div>

  <div class="row">

    <div class="col-xl-12 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Perhatian</div>
              <div class="mb-0 text-gray-800">Harap melakukan konfirmasi transfer agar kami dapat memverifikasi donasi, klik tombol <button class="btn btn-success btn-sm btn-circle"><i class="fa fa-check-circle"></i></button> untuk melakukan konfirmasi, Terima Kasih.</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-info fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Donasi</h6>
        </div>
        <div class="card-body">

        <div class="table-responsive">
          <table class="table table-hover table-bordered datatable">
            <thead>                                 
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Telepon</th>
                <th>Nominal</th>
                <th>Kategori</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('vendor/sb-temp/vendor/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('donation.index.d') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'nominal_rupiah', name: 'nominal'},
            {data: 'category', name: 'category'},
            {data: 'display_status', name: 'display_status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });
</script>
@endsection
