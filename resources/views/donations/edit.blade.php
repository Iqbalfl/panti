@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Donasi</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Edit Donasi</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('donation.update', $donation->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="row">
              <div class="form-group col-6 {{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title">Panggilan</label>
                <select  name="title" class="form-control @if ($errors->has('title')) is-invalid @endif" tabindex="1">
                    <option @if ($donation->title == "Bapak") selected @endif>Bapak</option>
                    <option @if ($donation->title == "Ibu") selected @endif>Ibu</option>
                    <option @if ($donation->title == "Saudara") selected @endif>Saudara</option>
                    <option @if ($donation->title == "Saudari") selected @endif>Saudari</option>
                </select>
                @if ($errors->has('title'))
                  <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                  </div>
                @endif
              </div>
              <div class="form-group col-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Nama Lengkap</label>
                <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $donation->name }}">
                @if ($errors->has('name'))
                  <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                  </div>
                @endif
              </div>
            </div>
            
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" tabindex="1" value="{{ $donation->email }}">
              @if ($errors->has('email'))
                <div class="invalid-feedback">
                  {{ $errors->first('email') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="phone">Nomor Telepon</label>
              <input id="phone" type="text" class="form-control @if ($errors->has('phone')) is-invalid @endif" name="phone" tabindex="1" value="{{ $donation->phone }}">
              @if ($errors->has('phone'))
                <div class="invalid-feedback">
                  {{ $errors->first('phone') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="address">Alamat</label>
              <textarea  id="address" type="text" class="form-control @if ($errors->has('address')) is-invalid @endif" name="address" tabindex="1">{{ $donation->address }}</textarea>
              @if ($errors->has('address'))
                <div class="invalid-feedback">
                  {{ $errors->first('address') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('nominal') ? ' has-error' : '' }}">
              <label for="nominal">Nominal</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Rp</span>
                </div>
                <input type="number" class="form-control  @if ($errors->has('nominal')) is-invalid @endif" name="nominal" aria-label="nominal" aria-describedby="basic-addon1" value="{{ $donation->nominal }}">
                @if ($errors->has('nominal'))
                  <div class="invalid-feedback">
                    {{ $errors->first('nominal') }}
                  </div>
                @endif
              </div>
            </div>

            <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
              <label for="category">Kategori</label>
              <select name="category" class="form-control @if ($errors->has('category')) is-invalid @endif" tabindex="1">
                <option @if ($donation->category == "Zakat") selected @endif>Zakat</option>
                <option @if ($donation->category == "Infaq") selected @endif>Infaq</option>
                <option @if ($donation->category == "Shodaqoh") selected @endif>Shodaqoh</option>
                <option @if ($donation->category == "Waqaf") selected @endif>Waqaf</option>
              </select>
              @if ($errors->has('category'))
                <div class="invalid-feedback">
                  {{ $errors->first('category') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('bank_id') ? ' has-error' : '' }}">
              <label for="bank_id">Transfer melalui bank</label>
              <select  name="bank_id" class="form-control @if ($errors->has('bank_id')) is-invalid @endif" tabindex="1">
                @foreach (\App\Bank::where('status', 100)->get() as $item)
                  <option value="{{ $item->id }}" @if ($donation->bank_id == $item->id) selected @endif>{{ $item->name.' : '.$item->account_number.'; AN. '.$item->account_holder }}</option>
                @endforeach
              </select>
              @if ($errors->has('bank_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('nominal') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection
