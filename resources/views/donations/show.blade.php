@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Donasi</h1> 
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow border-bottom-primary mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Detail Donasi</h6>
        </div>
        <div class="card-body">

          <p>
            Kode Donasi : <br><strong>#{{ $donation->code }}</strong>
            <hr>
          </p>
          <p>
            Nama Donatur : <br><strong>{{ $donation->title.' '.$donation->name }}</strong>
            <hr>
          </p>
          <p>
            Donatur Mempunyai Akun : <br>
            <strong>
              @if ($donation->user_id != null)
                YA
              @else
                TIDAK
              @endif
            </strong>
            <hr>
          </p>
          <p>
            Email : <br><strong>{{ $donation->email }}</strong>
            <hr>
          </p>
          <p>
            Nomor telepon : <br><strong>{{ $donation->phone }}</strong>
            <hr>
          </p>
          <p>
            Alamat : <br><strong>{{ $donation->address }}</strong>
            <hr>
          </p>
          <p>
            Nominal : <br><strong>{{ $donation->nominal_rupiah }}</strong>
            <hr>
          </p>
          <p>
            Kategori : <br><strong>{{ $donation->category }}</strong>
            <hr>
          </p>
          <p>
            Transfer Melalui Bank : <br><strong>{{ $donation->bank->name }}</strong>
            <hr>
          </p>
          <p>
            Status Donasi : <br><strong>{{ $donation->display_status }}</strong>
            <hr>
          </p>
        
        </div>
      </div>
    </div>

    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow border-bottom-primary mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Data Konfirmasi</h6>
        </div>
        <div class="card-body">

          <div class="table-responsive">
            <table class="table table-hover table-bordered datatable">
              <thead>                                 
                <tr>
                  <th>Ke</th>
                  <th>Bank</th>
                  <th>Atas Nama</th>
                  <th>Nominal Transfer</th>
                  <th>Tanggal</th>
                  <th>Bukti Transfer</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($donation->confirmations as $key => $item)
                  <tr>
                    <td>{{++$key}}</td>
                    <td>{{$item->bank->name}}</td>
                    <td>{{$item->account_holder}}</td>
                    <td>{{$item->nominal_rupiah}}</td>
                    <td>{{ date('d-m-Y', strtotime($item->date))}}</td>
                    <td><a href="{{asset('uploads/images/confirmations/'.$item->image)}}" target="_blank" class="btn btn-sm btn-warning text-white"><i class="fas fa-image"></i> Lihat</a></td>
                    <td>{{$item->display_status}}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="7" align="center">
                        <i>Belum ada data konfirmasi</i>
                    </td>
                  </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        
        </div>
      </div>
    </div>

  </div>
@endsection
