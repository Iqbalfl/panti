<?php


	return [

		'donation_status' => [
	        '10' => 'Dibatallkan',
	        '100' => 'Menunggu Konfirmasi',
	        '200' => 'Telah Diterima',
	    ],

	    'confirmation_status' => [
	        [ 
	        	'number' => 'all',
	        	'string' => 'Semua'
	        ],
	        [ 
	        	'number' => 10,
	        	'string' => 'Dibatalkan'
	        ],
	        [ 
	        	'number' => 100,
	        	'string' => 'Menunggu Konfirmasi'
	        ],
	        [ 
	        	'number' => 200,
	        	'string' => 'Telah Diterima'
	        ]
	    ]

	];