<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Frontend Area - Start */

Route::get('/', 'FrontendController@landing');
Route::get('/donasi', 'FrontendController@donation')->name('fe.donate');
Route::post('/donasi', 'FrontendController@donationStore')->name('fe.donate.store');
Route::get('/konfirmasi-transfer', 'FrontendController@confirmation')->name('fe.confirmation');
Route::post('/konfirmasi-transfer', 'FrontendController@confirmationStore')->name('fe.confirmation.store');
Route::get('/kontak', 'FrontendController@contact')->name('fe.contact');
Route::post('/kontak', 'FrontendController@contactStore')->name('fe.contact.store');
Route::get('/gallery', 'FrontendController@galleryIndex')->name('fe.gallery');
Route::get('/post', 'FrontendController@postIndex')->name('fe.post');
Route::get('/post/{slug}', 'FrontendController@postShow')->name('fe.post.show');
Route::get('/halaman/{slug}', 'FrontendController@pageShow')->name('fe.page.show');
Route::get('/data-anak-panti', 'FrontendController@member')->name('fe.member');

/* Frontend Area - End */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'main', 'middleware'=>'auth'], function () {

    Route::get('/gallery', 'GalleryController@index')->name('gallery.index')->middleware('role:admin');
    Route::delete('/gallery', 'GalleryController@destroy')->name('gallery.destroy')->middleware('role:admin');
    Route::post('/dropzone', 'GalleryController@dropZoneStore')->name('dropzone.store')->middleware('role:admin');
    Route::delete('/dropzone', 'GalleryController@dropZoneDestroy')->name('dropzone.destroy')->middleware('role:admin');

    Route::get('/confirmation', 'ConfirmationController@index')->name('confirmation.index')->middleware('role:admin');
    Route::patch('/confirmation/{id}/approve', 'ConfirmationController@approve')->name('confirmation.approve')->middleware('role:admin');
    Route::get('/confirmation/{id}/cancel', 'ConfirmationController@cancel')->name('confirmation.cancel')->middleware('role:admin');

    Route::get('/profile', 'UserController@showProfile')->name('profile.show');
    Route::patch('/profile', 'UserController@updateProfile')->name('profile.update');

    Route::resource('/page', 'PageController')->middleware('role:admin');
    Route::resource('/contact', 'ContactController')->middleware('role:admin');
    Route::resource('/category', 'CategoryController')->middleware('role:admin');
    Route::resource('/post', 'PostController')->middleware('role:admin');
    Route::resource('/bank', 'BankController')->middleware('role:admin');
    Route::resource('/donation', 'DonationController')->middleware('role:admin');
    Route::resource('/member', 'MemberController')->middleware('role:admin');
    Route::resource('/user', 'UserController')->middleware('role:admin');

    Route::get('/d/donation', 'DonaturController@indexDonation')->name('donation.index.d')->middleware('role:donatur');
    Route::get('/d/donation/create', 'DonaturController@createDonation')->name('donation.create.d')->middleware('role:donatur');
    Route::post('/d/donation', 'DonaturController@storeDonation')->name('donation.store.d')->middleware('role:donatur');
    Route::get('/d/donation/{id}/show', 'DonaturController@showDonation')->name('donation.show.d')->middleware('role:donatur');

    Route::get('/d/donation/{donation_id}/confirmation/create', 'DonaturController@createConfirmation')->name('confirmation.create.d')->middleware('role:donatur');
    Route::post('/d/donation/confirmation/store', 'DonaturController@storeConfirmation')->name('confirmation.store.d')->middleware('role:donatur');
});

